
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-4">
        <h2><?php echo e($title); ?></h2>
        <ol class="breadcrumb">
	        <li>
	            <a href="#">Bảng tin</a>
	        </li>
	        <li>
	            <a href="<?php echo e($route_list); ?>">Danh sách</a>
	        </li>
	        <li class="active">
	            <strong><?php echo e($title); ?></strong>
	        </li>
        </ol>
    </div>
</div>
<?php /**PATH C:\xampp\htdocs\evisa\resources\views/admin/partials/breadcrumb-form.blade.php ENDPATH**/ ?>