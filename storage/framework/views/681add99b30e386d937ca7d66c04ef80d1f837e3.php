<?php $__env->startSection('css'); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<?php if(session('error-customer')): ?>
<div class="alert alert-danger">
	<strong><?php echo e(session('error-customer')); ?></strong>
</div>
<?php endif; ?>
<?php if(session('success-customer')): ?>
<div class="alert alert-success">
	<strong><?php echo e(session('success-customer')); ?></strong>
</div>
<?php endif; ?>

<div class="table-responsive">
<table class="table table-striped table-bordered table-hover dataTables-example">
<thead style="color: #000;font-weight: bold;">
	<tr>
		<td class="text-center">#</td>
		<td class="text-center">Họ tên</td>
		<td class="text-center">Ngày đăng ký</td>
		<td class="text-center">Tình trạng</td>
		<td class="text-center">Dịch vụ</td>
		<td class="text-center">Tác vụ</td>
	</tr>
</thead>
<tbody>
	<?php $__currentLoopData = $objs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$obj): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	<tr>
		<td class="text-center"><?php echo e($key+1); ?></td>
		<td class="text-center"><?php echo e($obj->name); ?></td>
		<td class="text-center"><?php echo e($obj->created_at); ?></td>
		<td class="text-center">
			<?php if($obj->status == 0): ?>
				<span class="badge badge-warning">Chưa phản hồi</span>
			<?php elseif($obj->status == 1): ?>
				<span class="badge badge-success">Đã phản hồi</span> 
			<?php elseif($obj->status == 2): ?>
				<span class="badge badge-danger">Từ chối</span> 
			<?php endif; ?>
		</td>
		<td class="text-center">
			<?php if($obj->type === "evisa"): ?>
				<span class="badge badge-default">Visa điện tử</span>
			<?php elseif($obj->type === "visa_arrival"): ?>
				<span class="badge badge-default">Visa cửa khẩu</span> 
			<?php endif; ?>
		</td>
		<td class="text-center">
			<button class="btn btn-success btn-sm "  type="button">
					<a style="color:#fff" href="<?php echo e(route('detailOrder',$obj->id)); ?>">Chi tiết</a>
					
			</button>
			<button class="btn btn-danger btn-sm status-button" data-action ="<?php echo e(route('update-status',$obj->id)); ?>" data-type="2" type="button">
					Xóa
				<span class="fa fa-trash" data-fa-transform="shrink-3"></span>
			</button>
			
		</td>
		
	</tr>
	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</tbody>
</table>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.table-default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\evisa\resources\views/admin/customer/list.blade.php ENDPATH**/ ?>