<?php

    $about_us="";
    $link_icon=[];
    $quick_link=[];
    $address=[];
    $contact_phone="";
    $contact_fax="";
    foreach ($footer as $key => $value) {
        if($value->keyword==="about_us")
        {
            $about_us=$value;
        }
        else if($value->keyword==="link_icon"){
            array_push($link_icon,$value);
        }
        else if($value->keyword==="quick_link"){
            array_push($quick_link,$value);
        }
        else if($value->keyword==="address"){
            array_push($address,$value);
        }
        else if($value->keyword==="online_contact_phone"){
            $contact_phone=$value;
        }
        else if($value->keyword==="online_contact_fax"){
            $contact_fax=$value;
        }
    }
 
?>
<footer>
    <div class="footer-1">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-lg-8 col-xs-12">
                    <div class="call-to-action">
                        <div class="title-footer-1">
                            <h3>Call to action</h3>
                        </div>
                        <div class="row">
                            <div class="col-sm-7 box-form-footer-1">
                                <form action="">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Name">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Email">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Phone">
                                    </div>
                                </form>
                            </div>
                            <div class="col-sm-5">
                                <div class="warring-info">
                                    <p>
                                        Thông tin của bạn chỉ được chúng tôi sử dụng cho việc gửi thông tin từ E-visa tới bạn và không được tiết lộ cho bên thứ ba.
                                    </p>
                                    <button>brochure download</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 col-lg-4 col-xs-12">
                    <div class="travel-tour">
                        <div class="title-footer-1">
                            <h3>Travel tour</h3>
                        </div>
                        <div>
                            <p><span style="color: #173D66; font-weight: bold;">Public for everyone</span></p>
                            <p><span style="color: #808082;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</span></p>
                            <a href="https://www.namlongtour.com">https://www.namlongtour.com</a>
                            <button>Find more</button>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    <div class="footer-2">
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <div class="title">
                        <h3>About Us</h3>
                    </div>
                    <p><?php echo e($about_us->content); ?></p>
                    <ul class="socical">
                        <?php $__currentLoopData = $link_icon; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li>
                                <a href="<?php echo e($item->content); ?>">
                                    <img src="<?php echo e(asset($item->name)); ?>" alt="">
                                </a>
                            </li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      
                        
                    </ul>
                </div>
                <div class="col-sm-4 quick-links">
                    <div class="title">
                        <h3>Quick links</h3>
                    </div>
                    <ul>
                        <?php $__currentLoopData = $quick_link; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <li>
                            <i class="fas fa-angle-right"></i><a href="<?php echo e($item->content); ?>"><?php echo e($item->name); ?></a>
                            </li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        
                    </ul>
                </div>
                <div class="col-sm-4 get-in-touch">
                    <div class="title">
                        <h3>Get in Touch</h3>
                    </div>
                    <p>
                        <?php $__currentLoopData = $address; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <span style="color: #e7b806"><i class="fas fa-home"></i> <?php echo e($item->name); ?></span> <?php echo e($item->content); ?><br/>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        
                    </p>
                    <p>
                        <span style="color: #e7b806">
                            <i class="fas fa-phone-alt"></i> Phone: <?php echo e($contact_phone->content); ?> <br/>
                            <i class="fas fa-fax"></i> Fax: <?php echo e($contact_fax->content); ?>

                        </span>
                    </p>
                </div>
            </div>
        </div>
    </div>
</footer><?php /**PATH C:\xampp\htdocs\evisa\resources\views/font-end/partials/footer.blade.php ENDPATH**/ ?>