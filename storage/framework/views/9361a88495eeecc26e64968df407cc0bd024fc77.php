<!DOCTYPE html>
<html lang="en">
<?php echo $__env->make('font-end.partials.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<body>

    <?php echo $__env->make('font-end.partials.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>


    <div class="main">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="bg-main-white">
                        <div class="beadcrumb">
                            <ul>
                                <li>
                                    <a href="<?php echo e(URL::to('/')); ?>">Home</a><span>&nbsp; /</span>
                                </li>
                                
                                <li>
                                    <a href="#" class="active">Step 2: Điền form thông tin</a>
                                </li>
                            </ul>
                        </div>
                        <div class="content-step-2">
                            <h3>E-VISA APPLY ONLINE</h3>
                            <div class="menu-tabs">
                                <ul>
                                    <li class="<?php echo e($step == 2 ? 'active' : ''); ?>" onclick="openCity('step1')">
                                        <a href="javascript:void(0)" data-toggle="tab" href="#step1">1. ADD APPLICANT FILL FORM</a>
                                    </li>
                                    <li class="<?php echo e($step == 3 ? 'active' : ''); ?>" onclick="openCity('step2')">
                                        <a href="javascript:void(0)" data-toggle="tab" href="#step2">2. SUDMIT TO PAY</a>
                                    </li>
                                    <li class="<?php echo e($step == 4 ? 'active' : ''); ?>" onclick="openCity('step3')">
                                        <a href="javascript:void(0)" data-toggle="tab" href="#step3">3. COMPLETE</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 tabs-left">
                                    <div class="content-tabs">
                                        <div id="step1" class="container-tabs" <?php echo e($step == 2 ? 'style=display:block;' : 'style=display:none;'); ?> >
                                            <div class="row">
                                                <div class="col-lg-7">
                                                    <div class="form-info">
                                                        <form class="form-horizontal" role="form" 
                                                        action="<?php echo e(route('create-visa')); ?>" method="POST" enctype="multipart/form-data">
                                                        <?php echo csrf_field(); ?>
                                                        <h4><img src="<?php echo e(asset('e-visa/images/icon-form.png')); ?>" alt=""> FILL ONLINE FORM</h4>
                                                        <input type="hidden" name="visa_id" value="<?php echo e(isset($visa_new) ? $visa_new->id : ''); ?>">
                                                        <input type="hidden" name="check" value="<?php echo e(isset($check) ? $check : Session::get('check')); ?>">
                                                        <div class="form-group row">
                                                            <label for="inputPassword" class="col-sm-5 col-form-label">Nationality <span style="color: #FF1D25">(as in Passport *)</span></label>
                                                            <div class="col-sm-7">
                                                                <select class="form-control" id="nation_id" name="nation_id" required>            
                                                                    <?php $__currentLoopData = $nations; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $nation): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                    <?php if(isset($visa_new)): ?>
                                                                    <option data-value="<?php echo e($nation->value); ?>" value="<?php echo e($nation->id); ?>" 
                                                                        <?php echo e($visa_new->nation_id == $nation->id ? 'selected' : ''); ?>>
                                                                        <?php echo e($nation->name); ?>

                                                                    </option>
                                                                    <?php else: ?>
                                                                    <option data-value="<?php echo e($nation->value); ?>" value="<?php echo e($nation->id); ?>" 
                                                                        <?php echo e(Session::get('nation') == $nation->id ? 'selected' : ''); ?>>
                                                                        <?php echo e($nation->name); ?>

                                                                    </option>
                                                                    <?php endif; ?>
                                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="inputPassword" class="col-sm-5 col-form-label">Purpose of Visa <span style="color: #FF1D25">(*)</span></label>
                                                            <div class="col-sm-7">
                                                                <select class="form-control" id="purpose_id" name="purpose_id" required>                 
                                                                    <?php $__currentLoopData = $purposes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $purpose): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                       <?php if(isset($visa_new)): ?>
                                                                        <option data-value="<?php echo e($purpose->value); ?>" value="<?php echo e($purpose->id); ?>" 
                                                                            <?php echo e($visa_new->purpose_id == $purpose->id ? 'selected' : ''); ?>>
                                                                            <?php echo e($purpose->name); ?>

                                                                        </option>
                                                                        <?php else: ?>
                                                                        <option data-value="<?php echo e($purpose->value); ?>" value="<?php echo e($purpose->id); ?>" 
                                                                            <?php echo e(Session::get('purpose') == $purpose->id ? 'selected' : ''); ?>>
                                                                            <?php echo e($purpose->name); ?>

                                                                        </option>
                                                                        <?php endif; ?>
                                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="inputPassword" class="col-sm-5 col-form-label">Type of Visa <span style="color: #FF1D25">(*)</span></label>
                                                            <div class="col-sm-7">
                                                                <select class="form-control" id="type_id" name="type_id" required>                 
                                                                    <?php $__currentLoopData = $types; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $type): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                                        <?php if(isset($visa_new)): ?>
                                                                            <option data-value="<?php echo e($type->value); ?>" value="<?php echo e($type->id); ?>" 
                                                                                <?php echo e($visa_new->type_id == $type->id ? 'selected' : ''); ?>>
                                                                                <?php echo e($type->name); ?>

                                                                            </option>
                                                                        <?php else: ?>
                                                                            <option data-value="<?php echo e($type->value); ?>" value="<?php echo e($type->id); ?>" 
                                                                                <?php echo e(Session::get('type') == $type->id ? 'selected' : ''); ?>>
                                                                                <?php echo e($type->name); ?>

                                                                            </option>
                                                                        <?php endif; ?>
                                                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="inputPassword" class="col-sm-5 col-form-label">Processing Time <span style="color: #FF1D25">(*)</span></label>
                                                            <div class="col-sm-7">
                                                             <select class="form-control"  id="processing_id" name="processing_id" required>                      
                                                                <?php $__currentLoopData = $pross; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pro): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>        
                                                                    <?php if(isset($visa_new)): ?>
                                                                        <option data-value="<?php echo e($pro->value); ?>" value="<?php echo e($pro->id); ?>" 
                                                                            <?php echo e($visa_new->processing_id == $pro->id ? 'selected' : ''); ?>>
                                                                            <?php echo e($pro->name); ?>

                                                                        </option>
                                                                        <?php else: ?>
                                                                        <option data-value="<?php echo e($pro->value); ?>" value="<?php echo e($pro->id); ?>" 
                                                                            <?php echo e(Session::get('processing') == $pro->id ? 'selected' : ''); ?>>
                                                                            <?php echo e($pro->name); ?>

                                                                        </option>
                                                                    <?php endif; ?>
                                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <h4><img src="<?php echo e(asset('e-visa/images/icon-form.png')); ?>" alt=""> APPLICANT DETAILS</h4>
                                                    <div class="form-group row">
                                                        <label class="col-sm-5 col-form-label">Tải ảnh cá nhân</label>
                                                        <div class="col-sm-7">
                                                            <input type="file" name="fileprofile" id="fileprofile">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                            <label class="col-sm-5 col-form-label">Tải ảnh hộ chiếu</label>
                                                            <div class="col-sm-7">
                                                                <input type="file" name="filepassport" id="filepassport">
                                                            </div>
                                                        </div>
                                                    <div class="form-group row">
                                                        <label class="col-sm-5 col-form-label">Name <span style="color: #FF1D25">(Fullname *)</span></label>
                                                        <div class="col-sm-7">
                                                            <input type="text" name="name" id="name"  class="form-control" placeholder="fill your full name" value="<?php echo e(isset($visa_new) ? $visa_new->name : old('name')); ?>" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-sm-5 col-form-label">Passport number <span style="color: #FF1D25">(*)</span></label>
                                                        <div class="col-sm-7">
                                                            <input type="text" name ="pass_number" id="pass_number" class="form-control" placeholder="fill your passport number" value="<?php echo e(isset($visa_new) ? $visa_new->pass_number : old('pass_number')); ?>" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="inputPassword" class="col-sm-5 col-form-label">Cửa khẩu vào Việt Nam <span style="color: #FF1D25">(*)</span></label>
                                                        <div class="col-sm-7">
                                                            <select class="form-control" name="cuakhau" id="cuakhau" required>
                                                                <option label=" "></option>
                                                                <option value="Nội Bài"  <?php echo e(isset($visa_new) ? ($visa_new->cuakhau == "Nội Bài" ? 'selected' : '') : ''); ?>>Nội Bài</option>
                                                                <option value="Sao Vàng" <?php echo e(isset($visa_new) ? ($visa_new->cuakhau == "Sao Vàng" ? 'selected' : '') : ''); ?>>Sao Vàng</option>
                                                                <option value="Móng Cái" <?php echo e(isset($visa_new) ? ($visa_new->cuakhau == "Móng Cái" ? 'selected' : '') : ''); ?>>Móng Cái</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-sm-5 col-form-label">Ngày dự định vào Việt Nam <span style="color: #FF1D25">(*)</span></label>
                                                        <div class="col-sm-7">
                                                            <input type="date" name="date_in" id="date_in" class="form-control" placeholder="01/01/2019" value="<?php echo e(isset($visa_new) ? $visa_new->date_in : old('date_in')); ?>" required>
                                                        </div>
                                                    </div>
                                                    <h4><img src="<?php echo e(asset('e-visa/images/icon-form.png')); ?>" alt="">CONTACT INFORMATION</h4>
                                                    <div class="form-group row">
                                                        <label class="col-sm-5 col-form-label">Số điện thoại <span style="color: #FF1D25">(*)</span></label>
                                                        <div class="col-sm-4">
                                                            <?php
                                                                echo countrySelector("VN", "my-country", "my-country", "form-control",countryArray());
                                                            ?>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <input type="text" name="phone" id="phone" class="form-control" placeholder="984 062 393" value="<?php echo e(isset($visa_new) ? $visa_new->phone : old('phone')); ?>" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-sm-5 col-form-label">Email <span style="color: #FF1D25">(*)</span></label>
                                                        <div class="col-sm-7">
                                                            <input type="text" name="email" id="email" class="form-control" placeholder="Email" value="<?php echo e(isset($visa_new) ? $visa_new->email : old('email')); ?>" required>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="form-group row">
                                                        <label for="inputPassword" class="col-sm-5 col-form-label">Yêu cầu đặc biệt</label>
                                                        <div class="col-sm-7">
                                                            <textarea class="form-control" rows="5" id="note" name="note" value="<?php echo e(isset($visa_new) ? $visa_new->note : old('note')); ?>"></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="btn-pay">
                                                        <button type="button" onclick="window.location.href='<?php echo e(route('home')); ?>'">QUAY LẠI
                                                        </button>
                                                        <button type="submit">TIẾP TỤC</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="col-lg-5 tabs-right">
                                            <div class="view-order">
                                                <h4><i class="fas fa-shopping-cart"></i> XEM LẠI CHI TIẾT ĐƠN HÀNG</h4>
                                                <div class="content-cart">
                                                    <table class="table table-borderless">
                                                        <tbody>
                                                            <tr>
                                                                <td style="font-weight: 600;color: #B01803; width: 40%;">Purpose of visit: </td>
                                                                <td class="cf_purpose"><?php echo e(isset($visa_new) ? ($visa_new->purpose()->first() != null ? $visa_new->purpose()->first()->name : "") : ""); ?></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="font-weight: 600;color: #B01803; width: 40%;">Visa type: </td>
                                                                <td class="cf_visa_type">
                                                                    <?php echo e(isset($visa_new) ? ($visa_new->type()->first() != null ? $visa_new->type()->first()->name : "") : ""); ?>

                                                                </td>
                                                            </tr>
                                                            
                                                            <tr>
                                                                <td style="font-weight: 600;color: #B01803; width: 40%;">Processing time:</td>
                                                                <td class="cf_process">
                                                                    <?php echo e(isset($visa_new) ? ($visa_new->processing()->first() != null ? $visa_new->processing()->first()->name : "") : ""); ?>

                                                                </td>
                                                            </tr>
                                                            
                                                            <tr>
                                                                <td style="font-weight: 600;color: #B01803; width: 40%;">Visa fee:</td>
                                                                <td>25$ x 1 person= 25$</td>
                                                            </tr>
                                                            <tr>
                                                                <td style="font-weight: 600;color: #B01803; width: 40%;">Services fee: </td>
                                                                <td  class="service_fee"> </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="font-weight: 600;color: #B01803; width: 40%;">Code: </td>
                                                                <td class="cf_code"></td>
                                                            </tr>
                                                              <tr>
                                                                <td style="font-weight: 600;color: #B01803; width: 40%;">Express fee: </td>
                                                                <td >0$</td>
                                                            </tr>
                                                            <tr style="border-top: 2px solid #B01803">
                                                                <td style="font-weight: 600;color: #B01803; width: 40%;">Total fee: </td>
                                                                <td class="total_fee">180 USD</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="step2" class="container-tabs"   <?php echo e($step == 3 ? 'style=display:block;' : 'style=display:none;'); ?> >
                                    <form class="form-horizontal" role="form" 
                                        action="<?php echo e(route('update-visa')); ?>" method="POST">
                                        <?php echo csrf_field(); ?>
                                    <div class="row">
                                        <div class="col-lg-7">
                                            <input type="hidden" name="visa_id" value="<?php echo e(isset($visa_new) ? $visa_new->id : ''); ?>">
                                            <input type="hidden" name="check" value="<?php echo e(isset($check) ? $check : Session::get('check')); ?>">
                                            <h6>Chọn phương thức thanh toán</h6>
                                            <div class="row">
                                                <div class="form-group col-lg-6">
                                                    <select class="form-control" id="paymentForm" name="type_tt">
                                                        <option value="1" <?php echo e(isset($visa_new) ? ($visa_new->type_tt == 1 ? 'selected' : '') : ''); ?>>Thanh toán tại văn phòng</option>
                                                        <option value="2"<?php echo e(isset($visa_new) ? ($visa_new->type_tt == 2 ? 'selected' : '') : ''); ?>>PayPal</option>
                                                        <option value="3"<?php echo e(isset($visa_new) ? ($visa_new->type_tt == 3 ? 'selected' : '') : ''); ?>>Chuyển khoản</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div id="transfer" class="content-pay-all content-pay">
                                                        <p style="margin: 0;">
                                                            <span style="color: #FFC200;font-weight: 500;">Trụ sở chính: </span><br/>
                                                            Địa chỉ: 33 phố Hàng Mắm – Phường Lý Thái Tổ - Quận Hoàn Kiếm – Tp.Hà Nội<br/>
                                                            Hotline:  <br/>

                                                            <span style="color: #FFC200;font-weight: 500;">Tp. Đà Nẵng: </span><br/>
                                                            Địa chỉ: 47 phố Nguyễn Chí Thanh - Quận Hải Châu – TP. Đà Nẵng<br/>
                                                            Hotline: <br/>
                                                            <span style="color: #FFC200;font-weight: 500;">Tp. HCM:</span> <br/>
                                                            Địa chỉ: Lầu 9 tòa nhà Vietcomreal – số 68 phố Nguyễn Huệ - Phường Bến Nghé – TP. HCM<br/>
                                                            Hotline:
                                                        </p>
                                                    </div>
                                                    <div id="card" class="content-pay-all content-pay-2" style="display: none">
                                                        <img src="<?php echo e(asset('e-visa/images/pay-card.png')); ?>" alt="">
                                                    </div>
                                                    <div id="golbal" class="content-pay-all content-pay" style="display: none">
                                                        <p style="margin: 0;">
                                                            <span style="color: #FFC200;font-weight: 500;">Quốc tế: </span><br/>
                                                            Name Acount : DOAN HAI NAM<br/>
                                                            Bank : VIetcombank - Branch Hanoi<br/>
                                                            Switch Code : BFTVVNVX002<br/>
                                                            TK USD :  -0021370442274<br/>

                                                            <span style="color: #FFC200;font-weight: 500;">Trong nước: </span><br/>
                                                            TK VNĐ: 0011003141779<br/>
                                                            Nam Acount: ĐOÀN HẢI NAM<br/>
                                                            Ngân hàng: Vietcombank – chi nhánh Hà Nội<br/>
                                                            Swith Code: BFTVVNX002<br/>
                                                            Nội Dung: Thanh toán visa điện tử/ visa cửa khẩu – mã hồ sơ số: E/A 12345<br/>
                                                        </p>
                                                    </div>
                                                    <div class="btn-pay">
                                                        <button type= "submit">TIẾP TỤC</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-5 tabs-right">
                                            <div class="view-order">
                                                    <h4><i class="fas fa-shopping-cart"></i> XEM LẠI CHI TIẾT ĐƠN HÀNG</h4>
                                                    <div class="content-cart">
                                                        <table class="table table-borderless">
                                                            <tbody>
                                                                <tr>
                                                                    <td style="font-weight: 600;color: #B01803; width: 40%;">Purpose of visit: </td>
                                                                    <td class="cf_purpose"><?php echo e(isset($visa_new) ? ($visa_new->purpose()->first() != null ? $visa_new->purpose()->first()->name : "") : ""); ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="font-weight: 600;color: #B01803; width: 40%;">Visa type: </td>
                                                                    <td class="cf_visa_type">
                                                                        <?php echo e(isset($visa_new) ? ($visa_new->type()->first() != null ? $visa_new->type()->first()->name : "") : ""); ?>

                                                                    </td>
                                                                </tr>
                                                                
                                                                <tr>
                                                                    <td style="font-weight: 600;color: #B01803; width: 40%;">Processing time:</td>
                                                                    <td class="cf_process">
                                                                        <?php echo e(isset($visa_new) ? ($visa_new->processing()->first() != null ? $visa_new->processing()->first()->name : "") : ""); ?>

                                                                    </td>
                                                                </tr>
                                                                
                                                                <tr>
                                                                    <td style="font-weight: 600;color: #B01803; width: 40%;">Visa fee:</td>
                                                                    <td>25$ x 1 person= 25$</td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="font-weight: 600;color: #B01803; width: 40%;">Services fee: </td>
                                                                    <td  class="service_fee"> </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="font-weight: 600;color: #B01803; width: 40%;">Code: </td>
                                                                    <td class="cf_code"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="font-weight: 600;color: #B01803; width: 40%;">Express fee: </td>
                                                                    <td >0$</td>
                                                                </tr>
                                                                <tr style="border-top: 2px solid #B01803">
                                                                    <td style="font-weight: 600;color: #B01803; width: 40%;">Total fee: </td>
                                                                    <td class="total_fee">180 USD</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                                    </form>
                                </div>
                                <div id="step3" class="container-tabs"  <?php echo e($step == 4 ? 'style=display:block;' : 'style=display:none;'); ?>>
                                    <div class="content-complate">
                                        <h6><i class="fas fa-check"></i> &nbsp;Chúc mừng Bạn đã hoàn thành apply online dịch vụ E-visa. Chúng tôi sẽ thông báo kết quả sớm nhất qua Email hoặc tại quản tra cứu kết quả</h6>
                                        <div class="view-order-complate">
                                            <h6><i class="fas fa-shopping-cart"></i> &nbsp;XEM LẠI CHI TIẾT ĐƠN HÀNG</h6>
                                            <div class="container">
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <div class="content-cart">
                                                            <table class="table table-borderless">
                                                                <tbody>
                                                                    <tr>
                                                                        <td style="font-weight: 600;color: #B01803; width: 40%;">Purpose of visit: </td>
                                                                        <td  class="cf_purpose"><?php echo e(isset($visa_new) ? ($visa_new->purpose()->first() != null ? $visa_new->purpose()->first()->name : "") : ""); ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="font-weight: 600;color: #B01803; width: 40%;">Visa type: </td>
                                                                        <td class="cf_visa_type"><?php echo e(isset($visa_new) ? ($visa_new->type()->first() != null ? $visa_new->type()->first()->name : "") : ""); ?></td>
                                                                    </tr>
                                                                    
                                                                    <tr>
                                                                        <td style="font-weight: 600;color: #B01803; width: 40%;">Processing time:</td>
                                                                        <td class="cf_process"><?php echo e(isset($visa_new) ? ($visa_new->processing()->first() != null ? $visa_new->processing()->first()->name : "") : ""); ?></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <table class="table table-borderless">
                                                            <tbody>
                                                                <tr>
                                                                    <td style="font-weight: 600;color: #B01803; width: 40%;">Number Person:</td>
                                                                    <td>1</td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="font-weight: 600;color: #B01803; width: 40%;">Visa fee:</td>
                                                                    <td>25$ x 1 person= 25$</td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="font-weight: 600;color: #B01803; width: 40%;">Services fee: </td>
                                                                    <td class="service_fee"> </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="font-weight: 600;color: #B01803; width: 40%;">Code: </td>
                                                                    <td class="cf_code"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="font-weight: 600;color: #B01803; width: 40%;">Express fee: </td>
                                                                    <td >0$</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <table class="table table-borderless">
                                                            <tbody>
                                                                <tr style="border-top: 2px solid #B01803">
                                                                    <td style="font-weight: 600;color: #B01803; font-size: 18px;">Total fee: </td>
                                                                    <td style="font-weight: 600;color: #B01803; font-size: 18px;" class="total_fee">180 USD</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-12 text-center">
                                                        <button style="background: #B01803; border: none; color: #fff; margin: 20px 0; text-align: center; padding: 10px 15px; border-radius: 5px;"
                                                        type="button" onclick="window.location.href='<?php echo e(route('home')); ?>'">VỀ TRANG CHỦ</button>
                                                        <button style="background: #4267b2; border: none; color: #fff; margin: 20px 0; text-align: center; padding: 10px 15px; border-radius: 5px;"
                                                        type="button" onclick="window.location.href='<?php echo e(route('home')); ?>'">DỊCH VỤ THÊM</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
    $arrType=json_encode($types);
    $arrNation=json_encode($nations);
    $arrPupose=json_encode($purposes);
    $arrProcess=json_encode($pross);
?>
</div>
<script>
        var check=<?php echo json_encode(isset($check)?$check:Session::get('check')); ?>;
        var service_support =0;
        var arrType=<?php echo $arrType; ?>;
        var arrNation=<?php echo $arrNation; ?>;
        var arrPupose=<?php echo $arrPupose; ?>;
        var arrProcess=<?php echo $arrProcess; ?>;
        
    </script>
<?php echo $__env->make('font-end.partials.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->make('font-end.partials.scripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</body>
</html><?php /**PATH C:\xampp\htdocs\evisa\resources\views/font-end/step2.blade.php ENDPATH**/ ?>