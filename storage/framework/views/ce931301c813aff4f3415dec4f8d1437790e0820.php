<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <?php echo $__env->make('admin.partials.navbar-left.nav-header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            <li class=<?php echo e($flag == "bangtin" ? "active" : ""); ?>>
                <a href="#"><i class="fa fa-th-large"></i> <span class="nav-label">Bảng tin</span></span></a>
            </li>
            <li class=<?php echo e($flag == "customer" ? "active" : ""); ?>>
                <a href="<?php echo e(route('customer.index')); ?>"><i class="fa fa-picture-o"></i> <span class="nav-label">Danh sách yêu cầu</span></a>
            </li>
            <li class=<?php echo e($flag == "nation" ? "active" : ""); ?>>
                <a href="<?php echo e(route('nation.index')); ?>"><i class="fa fa-picture-o"></i> <span class="nav-label">Nationality</span></a>
            </li>
            <li class=<?php echo e($flag == "purpose" ? "active" : ""); ?>>
                <a href="<?php echo e(route('purpose.index')); ?>"><i class="fa fa-picture-o"></i> <span class="nav-label">Purpose of Visa</span></a>
            </li>
            <li class=<?php echo e($flag == "type" ? "active" : ""); ?>>
                <a href="<?php echo e(route('type.index')); ?>"><i class="fa fa-picture-o"></i> <span class="nav-label">Type of Visa</span></a>
            </li>
            <li class=<?php echo e($flag == "processing" ? "active" : ""); ?>>
                <a href="<?php echo e(route('processing.index')); ?>"><i class="fa fa-picture-o"></i> <span class="nav-label">Processing Time</span></a>
            </li>
            <li class=<?php echo e($flag == "files" ? "active" : ""); ?>>
                <a href="<?php echo e(route('gallery')); ?>"><i class="fa fa-picture-o"></i> <span class="nav-label">Files</span></a>
            </li>
            <li class=<?php echo e($flag == "header" ? "active" : ""); ?>>
                    <a href="<?php echo e(route('indexHeader')); ?>"><i class="fa fa-picture-o"></i> <span class="nav-label">Theme Option</span></a>
            </li>
            <li class="landing_link">
                <a target="_blank" href="<?php echo e(config('website.domain')); ?>"><i class="fa fa-star"></i> <span class="nav-label">Website</span> <span class="label label-warning pull-right"></span></a>
            </li>
        </ul>
    </div>
</nav><?php /**PATH C:\xampp\htdocs\evisa\resources\views/admin/partials/left-side-bar.blade.php ENDPATH**/ ?>