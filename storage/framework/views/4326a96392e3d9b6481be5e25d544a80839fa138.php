<?php $__env->startSection('title'); ?>
Chi tiết đơn hàng
##parent-placeholder-3c6de1b7dd91465d437ef415f94f36afc1fbc8a8##
<?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?>
<link href="<?php echo e(asset('assets/css/plugins/datapicker/datepicker3.css')); ?>" rel="stylesheet">
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>

<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-4">
        <h2>Chi tiết đơn hàng</h2>
        <ol class="breadcrumb">
	        <li>
	            <a href="<?php echo e(route('home')); ?>">Home</a>
	        </li>
	        <li>
	            
	        </li>
	        <li class="active">
	            <strong>Chi tiết đơn hàng</strong>
	        </li>
        </ol>
    </div>
</div>



<div class="wrapper wrapper-content">
	<div class="row animated fadeInRight">
		<div class="ibox float-e-margins">
			<div class="ibox-title">
	            <h5>Thông tin</h5>
	            <div class="ibox-tools">
		            <a class="collapse-link">
		                <i class="fa fa-chevron-up"></i>
		            </a>
	            </div>
	        </div>
	        <div class="ibox-content">
					<form  class="form-horizontal">
							<div class="form-group">
								<div class="col-12 text-center" style="background:#1cc09f;color:#fff;padding:3px;font-size:17px">
										<label class=" control-label">FORM </label>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Nationality : </label>
								<div class="col-sm-10">
									<input type="text" class="form-control" name="address" disabled id="address" value="<?php echo e($order['nation']->name); ?>">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Purpose of Visa: </label>
								<div class="col-sm-10">
									<input type="text" class="form-control" name="purpose" disabled id="purpose" value="<?php echo e($order['purpose']->name); ?>">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Type of Visa : </label>
								<div class="col-sm-10">
									<input type="text" class="form-control" name="type" disabled id="type" value="<?php echo e($order['type']->name); ?>">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Processing Time: </label>
								<div class="col-sm-10">
									<input type="text" class="form-control" name="processing" disabled id="processing" value="<?php echo e($order['processing']->name); ?>">
								</div>
							</div>
							<div class="form-group">
								<div class="col-12 text-center" style="background:#1cc09f;color:#fff;padding:3px;font-size:17px">
										<label class=" control-label">APPLICANT DETAILS </label>
								</div>
							</div>
							<?php if($order['fileprofile'] ): ?>
								<div class="hr-line-dashed"></div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Ảnh cá nhân</label>
		
									<div class="col-sm-10">
										<img src="<?php echo e(asset($order['fileprofile'])); ?>" alt="imagereview" id="imagereview" style="width:50%;height:50%">
									</div>
								</div>
							<?php endif; ?>
							<?php if($order['filepassport'] ): ?>
								<div class="hr-line-dashed"></div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Ảnh hộ chiếu</label>
		
									<div class="col-sm-10">
										<img src="<?php echo e(asset($order['filepassport'])); ?>" alt="imagereview" id="imagereview" style="width:50%;height:50%">
									</div>
								</div>
							<?php endif; ?>
						<div class="form-group">
							<label class="col-sm-2 control-label">Tên: </label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="name" disabled id="name" value="<?php echo e($order['name']); ?>">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Passport number: </label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="pass_number" disabled id="pass_number" value="<?php echo e($order['pass_number']); ?>">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Cửa khẩu: </label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="cuakhau" disabled id="cuakhau" value="<?php echo e($order['cuakhau']); ?>">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Ngày vào VN: </label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="date_in" disabled id="date_in" value="<?php echo e($order['date_in']); ?>">
							</div>
						</div>
						<div class="form-group">
							<div class="col-12 text-center" style="background:#1cc09f;color:#fff;padding:3px;font-size:17px">
									<label class=" control-label">CONTACT INFORMATION </label>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Số điện thoại: </label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="phone" disabled id="phone" value="<?php echo e($order['phone']); ?>">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Email: </label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="email" disabled id="email" value="<?php echo e($order['email']); ?>">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Ghi chú: </label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="note" disabled id="note" value="<?php echo e($order['note']); ?>">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Hình thức thanh toán : </label>
							<?php if($order['type_tt']==1): ?>
								<div class="col-sm-10">
									<input type="text" class="form-control" name="type_tt" disabled id="type_tt" value="Thanh toán tại văn phòng">
								</div>
							<?php elseif($order['type_tt']==2): ?>
								<div class="col-sm-10">
									<input type="text" class="form-control" name="type_tt" disabled id="type_tt" value="Paypal">
								</div>
							<?php elseif($order['type_tt']==3): ?>
								<div class="col-sm-10">
									<input type="text" class="form-control" name="type_tt" disabled id="type_tt" value="Chuyển khoản">
								</div>
							<?php endif; ?>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Trạng thái đơn hàng: </label>
							<?php if($order['status']==0): ?>
								<div class="col-sm-10">
									<input type="text" class="form-control" name="status" disabled id="status" value="Chưa phản hồi">
								</div>
							<?php elseif($order['status']==1): ?>
								<div class="col-sm-10">
									<input type="text" class="form-control" name="status" disabled id="status" value="Đã phản hồi">
								</div>
							<?php endif; ?>
						</div>
						
					</div>
				
            	</form>
				
	        </div>
		</div>
	</div>		
</div>


<?php $__env->stopSection(); ?>


<?php $__env->startSection('script'); ?>
<script src="<?php echo e(asset('assets/js/plugins/datapicker/bootstrap-datepicker.js')); ?>"></script>
<script>
    $(document).ready(function(){
		function readURL(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();
				
				reader.onload = function(e) {
					$('#imagereview').attr('src', e.target.result);
				}
				
				reader.readAsDataURL(input.files[0]);
			}
		}

		$("#image").change(function() {
			console.log("XXX");
			readURL(this);
		});
    });
</script>	
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\evisa\resources\views/admin/customer/detail.blade.php ENDPATH**/ ?>