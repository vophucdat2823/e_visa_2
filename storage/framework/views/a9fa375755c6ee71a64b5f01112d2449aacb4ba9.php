<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>INSPINIA | Login</title>

    <link href="<?php echo e(asset('assets/css/bootstrap.min.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('assets/font-awesome/css/font-awesome.css')); ?>" rel="stylesheet">

    <link href="<?php echo e(asset('assets/css/animate.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('assets/css/style.css')); ?>" rel="stylesheet">

</head>

<body class="gray-bg">

    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div>

               <img alt="image" class="img-circle" src="<?php echo e(asset('assets/img/logo-default.png')); ?>" />

            </div>
            <h3>Chào mừng đến hệ thống quản lý DNM</h3>
           
            <p>Đăng nhập để sử dụng hệ thống</p>
            <form method="POST" class="m-t" action="<?php echo e(route('login')); ?>">
               <?php echo csrf_field(); ?>

               <div class="form-group row">
                <input id="email" type="email" class="form-control <?php if ($errors->has('email')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('email'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" name="email" value="<?php echo e(old('email')); ?>" required autocomplete="email" placeholder="Email" autofocus>

                <?php if ($errors->has('email')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('email'); ?>
                <span class="invalid-feedback" role="alert">
                    <strong><?php echo e($message); ?></strong>
                </span>
                <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
            </div>

            <div class="form-group row">
             <input id="password" type="password" class="form-control <?php if ($errors->has('password')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('password'); ?> is-invalid <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>" name="password" required autocomplete="current-password" placeholder="Mật khẩu">

             <?php if ($errors->has('password')) :
if (isset($message)) { $messageCache = $message; }
$message = $errors->first('password'); ?>
             <span class="invalid-feedback" role="alert">
                <strong><?php echo e($message); ?></strong>
            </span>
            <?php unset($message);
if (isset($messageCache)) { $message = $messageCache; }
endif; ?>
        </div>

        <button type="submit" class="btn btn-primary block full-width m-b">Đăng nhập</button>
        <?php if(Route::has('password.request')): ?>
        <a class="btn btn-link" href="<?php echo e(route('password.request')); ?>">
            <?php echo e(__('Quên mật khẩu?')); ?>

        </a>
        <?php endif; ?>


        <p class="text-muted text-center"><small>Chưa có tài khoản?</small></p>
        <a class="btn btn-sm btn-white btn-block" href="register.html">Tạo tài khoản mới</a>
    </form>
</div>
</div>

<!-- Mainly scripts -->
<script src="<?php echo e(asset('assets/js/jquery-2.1.1.js')); ?>"></script>
<script src="<?php echo e(asset('assets/js/bootstrap.min.js')); ?>"></script>

</body>

</html>
<?php /**PATH C:\xampp\htdocs\evisa\resources\views/auth/login.blade.php ENDPATH**/ ?>