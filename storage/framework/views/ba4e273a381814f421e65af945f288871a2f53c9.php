<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-4">
		<h2><?php echo e($title); ?></h2>
		<ol class="breadcrumb">
			<li>
				<a href="#">Bảng tin</a>
			</li>
			<li class="active">
				<strong><?php echo e($title); ?></strong>
			</li>
		</ol>
	</div>
	<div class="col-sm-8">
		<div class="title-action">
			<a href="<?php echo e($route_create); ?>" class="btn btn-primary">Thêm mới</a>
		</div>
	</div>
</div><?php /**PATH C:\xampp\htdocs\evisa\resources\views/admin/partials/breadcrumb-table.blade.php ENDPATH**/ ?>