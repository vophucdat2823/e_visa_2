<?php $__env->startSection('css'); ?>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
<?php if(session('error-nation')): ?>
<div class="alert alert-danger">
	<strong><?php echo e(session('error-nation')); ?></strong>
</div>
<?php endif; ?>
<?php if(session('success-nation')): ?>
<div class="alert alert-success">
	<strong><?php echo e(session('success-nation')); ?></strong>
</div>
<?php endif; ?>

<div class="table-responsive">
<table class="table table-striped table-bordered table-hover dataTables-example">
<thead style="color: #000;font-weight: bold;">
	<tr>
		<td class="text-center">ID</td>
		<td class="text-center">Tên</td>
		<td class="text-center">Value</td>
		<td class="text-center">Type</td>
		<td class="text-center">Trạng thái</td>
		<td class="text-center"></td>
	</tr>
</thead>
<tbody>
	<?php $__currentLoopData = $objs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $obj): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	<tr>
		<td class="text-center"><?php echo e($obj->id); ?></td>
		<td class="text-center"><?php echo e($obj->name); ?></td>
		<td class="text-center"><?php echo e($obj->value); ?></td>
		<td class="text-center">
			<?php if($obj->type_evisa == 1 && $obj->type_visa_arrival !=1): ?>
				<span class="badge badge-success">Evisa</span>
			<?php elseif($obj->type_evisa != 1 && $obj->type_visa_arrival ==1): ?>
				<span class="badge badge-success">Visa cửa khẩu</span>
			<?php else: ?>
				<span class="badge badge-success">Cả 2 loại</span> 
			<?php endif; ?>
		</td>
		<td class="text-center">
			<?php if($obj->status == 1): ?>
				<span class="badge badge-success">Sử dụng</span>
			<?php else: ?>
				<span class="badge badge-danger">Không sử dụng</span> 
			<?php endif; ?>
		</td>
		<td class="text-center">
			<form action="<?php echo e(route('nation.edit', ['id' => $obj->id])); ?>">
				<button class="btn btn-warning btn-sm" type="submit">
						Sửa
				<span class="fa fa-edit" data-fa-transform="shrink-3"></span>
				</button>
				<button class="btn btn-danger btn-sm delete-button" 
					data-action ="<?php echo e(route('nation.destroy',$obj->id)); ?>" type="button">
						Xóa
				<span class="fa fa-trash" data-fa-transform="shrink-3"></span>
				</button>	
			</form>
		</td>
	</tr>
	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</tbody>
</table>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startSection('js'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.table-default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\evisa\resources\views/admin/nation/list.blade.php ENDPATH**/ ?>