<?php $__env->startSection('title'); ?>
Quản lý giao diện người dùng
##parent-placeholder-3c6de1b7dd91465d437ef415f94f36afc1fbc8a8##
<?php $__env->stopSection(); ?>

<?php $__env->startSection('css'); ?>
<link href="<?php echo e(asset('assets/css/plugins/iCheck/custom.css')); ?>" rel="stylesheet">
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>

<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-4">
        <h2>Quản lý giao diện người dùng</h2>
        <ol class="breadcrumb">
	        <li>
	            <a href="index.html">Home</a>
	        </li>
	        <li class="active">
	            <strong>Quản lý giao diện người dùng</strong>
	        </li>
        </ol>
    </div>
    <div class="col-sm-8">
        <div class="title-action">
          
        </div>
    </div>
</div>


<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox float-e-margins">
				
				<div class="ibox-title">
					<h5>Quản lý giao diện người dùng</h5>
					<div class="ibox-tools">
						<a class="collapse-link">
							<i class="fa fa-chevron-up"></i>
						</a>						
					</div>
				</div>
				
				<div class="ibox-content">
					
					
					<div class="table-responsive">
							<?php if(session('success-theme')): ?>
								<div class="alert alert-success">
									<strong><?php echo e(session('success-theme')); ?></strong>
								</div>
							<?php endif; ?>
						<table class="table table-striped">
							<thead>
								<tr>
									<th>Tên </th>
									<th>Nội dung </th>
									<th>Tác vụ</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td colspan="3" style="background-color:#1ab394;color: #fff;" class="text-center">HEADER</td>
								</tr>
								<?php $__currentLoopData = $theme_header; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<tr>
										<td><?php echo e($item->name); ?></td>
										<td>
											<?php if($item->keyword==='banner' || $item->keyword==='logo'): ?>
												<img width="200" src="<?php echo e(asset($item->content)); ?>" alt="">
											<?php else: ?>
												<?php echo e($item->content); ?>

											<?php endif; ?>
										</td>
										<td>
											<a href="<?php echo e(url('admin/theme-option/edit?id='.$item->id)); ?>" class="btn btn-warning btn-circle"><i class="fa fa-edit"></i></a>
										</td>
									</tr>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>	
								<tr>
									<td colspan="3" style="background-color:#1ab394;color: #fff;" class="text-center">FOOTER</td>
								</tr>
								<?php $__currentLoopData = $theme_footer; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<tr>
										<td><?php echo e($item->name); ?></td>
										<td><?php echo e($item->content); ?></td>
										<td>
											<a href="<?php echo e(url('admin/theme-option/edit?id='.$item->id)); ?>" class="btn btn-warning btn-circle"><i class="fa fa-edit"></i></a>
										</td>
									</tr>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								<tr>
									<td colspan="3" style="background-color:#1ab394;color: #fff;" class="text-center">LINK ICON</td>
								</tr>
								<?php $__currentLoopData = $link_icon; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<tr>
										<td> <img width="50" src="<?php echo e(asset($item->name)); ?>" alt=""></td>
										<td><?php echo e($item->content); ?></td>
										<td>
											<a href="<?php echo e(url('admin/theme-option/edit?id='.$item->id)); ?>" class="btn btn-warning btn-circle"><i class="fa fa-edit"></i></a>
										</td>
									</tr>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
								<tr>
									<td colspan="3" style="background-color:#1ab394;color: #fff;" class="text-center">QUICK LINK</td>
								</tr>
								<?php $__currentLoopData = $quick_link; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
									<tr>
										<td><?php echo e($item->name); ?></td>
										<td><?php echo e($item->content); ?></td>
										<td>
											<a href="<?php echo e(url('admin/theme-option/edit?id='.$item->id)); ?>" class="btn btn-warning btn-circle"><i class="fa fa-edit"></i></a>
										</td>
									</tr>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>															
							</tbody>
						</table>
					</div>
				
				</div>
			</div>
		</div>
	</div>
</div>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('script'); ?>
	
<!-- iCheck -->
<script src="<?php echo e(asset('assets/js/plugins/iCheck/icheck.min.js')); ?>"></script>

<!-- Peity -->
<script src="<?php echo e(asset('assets/js/demo/peity-demo.js')); ?>"></script>
<script>
    $(document).ready(function(){
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
         });
    });
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\evisa\resources\views/admin/theme/index.blade.php ENDPATH**/ ?>