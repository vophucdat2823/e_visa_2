 <li class="nav-header">
    <div class="dropdown profile-element"> 
        <span>
            <img alt="image" class="img-circle" src="<?php echo e(asset('assets/img/user-default.png')); ?>" />
        </span>
        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold"><?php echo e(Auth::user()->name); ?></strong>
                <span class="clear"> <span class="block m-t-xs"> <small class="font-bold"><?php echo e(Auth::user()->email); ?></small>
                </span> <span class="text-muted text-xs block">Admin <b class="caret"></b></span> </span> </a>
                <ul class="dropdown-menu animated fadeInRight m-t-xs">
                    <li><a href="#">Profile</a></li>
                    <li><a href="#">Contacts</a></li>
                    <li><a href="#">Mailbox</a></li>
                    <li class="divider"></li>
                    <li><a href="<?php echo e(route('logout')); ?>"  onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">Logout</a>
                    <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
                       <?php echo csrf_field(); ?>
                   </form>

               </li>
           </ul>
       </div>
       <div class="logo-element">
        DNM
    </div>
</li>
<?php /**PATH C:\xampp\htdocs\evisa\resources\views/admin/partials/navbar-left/nav-header.blade.php ENDPATH**/ ?>