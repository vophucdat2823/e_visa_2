<?php if(count($errors) > 0): ?>
<div class="alert alert-danger">
	<?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $err): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	<strong><?php echo e($err); ?></strong><br>
	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</div>
<?php endif; ?>
<?php /**PATH C:\xampp\htdocs\evisa\resources\views/admin/partials/error-list.blade.php ENDPATH**/ ?>