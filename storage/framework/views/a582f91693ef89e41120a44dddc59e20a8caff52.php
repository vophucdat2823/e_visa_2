<?php $__env->startSection('title'); ?>
Lỗi không tìm thấy
##parent-placeholder-3c6de1b7dd91465d437ef415f94f36afc1fbc8a8##
<?php $__env->stopSection(); ?>


<?php $__env->startSection('content'); ?>
<div class="middle-box text-center animated fadeInDown">
    <h1>404</h1>
    <h3 class="font-bold">Không tìm thấy trang</h3>

    <div class="error-desc">
        Xin lỗi, hệ thống không thể tìm thấy thông tin trang bạn yêu cầu.Vui lòng kiểm tra lại thông tin hoặc
        liên lạc với admin hệ thống! </br>
        Xin cảm ơn.</br></br></br>
       <a href="<?php echo e(route('customer.index')); ?>" class="btn btn-primary">Quay về trang chủ</a>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\evisa\resources\views/errors/404.blade.php ENDPATH**/ ?>