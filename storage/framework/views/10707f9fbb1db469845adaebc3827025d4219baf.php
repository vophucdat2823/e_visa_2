<?php if(session('error-nation')): ?>
<div class="alert alert-danger">
	<strong><?php echo e(session('error-nation')); ?></strong>
</div>
<?php endif; ?>
<?php if(session('success-nation')): ?>
<div class="alert alert-success">
	<strong><?php echo e(session('success-nation')); ?></strong>
</div>
<?php endif; ?>
<div class="form-group <?php echo e($errors->has('name') ? 'has-error' : ''); ?>">
	<label class="col-sm-2 control-label">Tên (*) </label>
	<div class="col-sm-4">
		<input type="text" class="form-control" name="name" id="name" 
		value="<?php echo e(isset($obj) ? $obj->name : old('name')); ?>" required>
	</div>
</div>
<div class="form-group <?php echo e($errors->has('name') ? 'has-error' : ''); ?>">
	
	<label class="col-sm-2 control-label">Loại (*) </label>
	<div class="col-sm-2">
		<div class="radio">
			<label>
			  <input type="radio" name="typevisa"  value="1" 
				<?php if(isset($obj) && $obj->type_evisa==1 && $obj->type_visa_arrival !=1): ?>
					checked
				<?php endif; ?>
			  >
			  Evisa
			</label>
		</div>
	</div>
	<div class="col-sm-2">
		<div class="radio">
			<label>
			  <input type="radio" name="typevisa"  value="2"
			  	<?php if(isset($obj) && $obj->type_visa_arrival==1 && $obj->type_evisa != 1): ?>
					checked
				<?php endif; ?>
			  >
			  Visa cửa khẩu
			</label>
		</div>
	</div>
	<div class="col-sm-2">
		<div class="radio">
			<label>
			  <input type="radio" name="typevisa" value="3" 
			  	<?php if(isset($obj) && $obj->type_visa_arrival == 1 && $obj->type_evisa==1): ?>
					checked
				<?php elseif(!isset($obj) ): ?>
					checked
				<?php endif; ?>
			  >
			 Cả 2 loại
			</label>
		</div>
	</div>
</div>
<div class="form-group <?php echo e($errors->has('value') ? 'has-error' : ''); ?>">
	<label class="col-sm-2 control-label">Giá trị (*) </label>
	<div class="col-sm-4">
		<input type="number" min="0" class="form-control" name="value" id="value" 
		value="<?php echo e(isset($obj) ? $obj->value : old('value')); ?>" required>
	</div>
</div>
<?php echo $__env->make('admin.partials.status', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\evisa\resources\views/admin/nation/form.blade.php ENDPATH**/ ?>