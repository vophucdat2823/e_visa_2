
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<title><?php echo e($title); ?></title>

<link href="<?php echo e(asset('assets/css/bootstrap.min.css')); ?>" rel="stylesheet">
<link href="<?php echo e(asset('assets/font-awesome/css/font-awesome.css')); ?>" rel="stylesheet">
<link href="<?php echo e(asset('assets/css/style.css')); ?>" rel="stylesheet">
<?php /**PATH C:\xampp\htdocs\evisa\resources\views/admin/partials/head.blade.php ENDPATH**/ ?>