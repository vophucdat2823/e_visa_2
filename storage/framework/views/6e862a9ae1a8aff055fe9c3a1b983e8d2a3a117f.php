<div class="form-group">
	<label class="col-sm-2 control-label">Trạng thái (*)</label>
	<div class="col-md-4">
		<?php if(!isset($obj)): ?>
			<select class="form-control m-b" name="status">
				<option value="1" <?php echo e(old('status') == "1" ? "selected" : ""); ?>>Sử dụng</option>
				<option value="0" <?php echo e(old('status') == "0" ? "selected" : ""); ?>>Không sử dụng</option>
			</select> 
		<?php else: ?>
			<select class="form-control m-b" name="status">
				<option value="1" <?php echo e($obj->status == "1" ? "selected" : ""); ?>>Sử dụng</option>
				<option value="0" <?php echo e($obj->status == "0" ? "selected" : ""); ?>>Không sử dụng</option>
			</select>
		<?php endif; ?>                                       
	</div>
</div><?php /**PATH C:\xampp\htdocs\evisa\resources\views/admin/partials/status.blade.php ENDPATH**/ ?>