<?php
    $banner="";
    $logo="";
    $hotline="";
    $mail="";
    foreach ($header as $key => $value) {
        if($value->keyword==="banner")
        {
            $banner=$header[$key];
        }
        else if($value->keyword==="logo"){
            $logo=$header[$key];
        }
        else if($value->keyword==="hotline"){
            $hotline=$header[$key];
        }
        else if($value->keyword==="mail"){
            $mail=$header[$key];
        }
    }
    
?>
 <div class="nav-mobile">
    <input type="hidden" value="<?php echo e(config('website.domain')); ?>" id="base_url">domain
    <div class="btn-toggle">
        <button><i class="fas fa-bars"></i></button>
    </div>
    <div class="menu-mobile">
        <ul>
            <li>
                <a href="<?php echo e(route('procedure-instructions')); ?>">Hướng dẫn</a>
            </li>
            <li>
                <a href="#">Gia hạn visa</a>
            </li>
            <li>
                <a href="#">Thẻ tạm trú</a>
            </li>
            <li>
                <a href="#">Dịch vụ</a>
            </li>
            <li>
                <a href="#">Theo dõi tiến độ</a>
            </li>
            <li>
                <a href="#">Liên hệ</a>
            </li>
            <li>
                <a href="#">Đăng nhập</a>
            </li>
            <li>
                <a href="#">Đăng ký</a>
            </li>
            <li>
                <a href="#">Tiếng Việt</a>
            </li>
            <li>
                <a href="#">English</a>
            </li>
        </ul>
    </div>
  
    <div class="logo-mobile">
        <a href="#">
            <img src="<?php echo e(asset($logo->content)); ?>" alt="">
        </a>
    </div>
    <div class="btn-search">
        <button><i class="fas fa-search"></i></button>
    </div>
    <div class="box-search-mobile">
        <form action="">
            <div class="input-group search-box">
                <input type="text" class="form-control" placeholder="Recipient's username" aria-label="Recipient's username" aria-describedby="basic-addon2">
                <div class="input-group-append">
                    <!-- <span >@example.com</span> -->
                    <button class="input-group-text" id="basic-addon2">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>
<header>
    <div id="header-1">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 any-question">
                    <ul>
                        <li>
                            <a href="#" class="text-yellow">Have any questions?</a>
                        </li>
                        <li>
                        <a href="#"><i class="fas fa-phone-alt text-yellow"></i> <?php echo e($hotline->content); ?></a>
                        </li>
                        <li>
                            <a href="#"><i class="far fa-envelope text-yellow"></i> <?php echo e($mail->content); ?></a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-6 col-sm-6 col-lg-3 col-xs-12 btn-login text-right">
                    <button>Register or Login using ID</button>
                </div>
                <div class="col-md-6 col-sm-6 col-lg-3 col-xs-12 language text-right">
                    <ul>
                        <li>
                            <a href="#">Tiếng việt</a>
                        </li>
                        <li>
                            <a href="#">English</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div id="header-2">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 logo">
                    <a href="<?php echo e(URL::to('/')); ?>">
                        <img src="<?php echo e(asset($logo->content)); ?>" alt="">
                    </a>
                </div>
                <div class="col-sm-8 pt-10">
                 <div class="menu-header-2">
                     <ul>
                         <li>
                             <a href="<?php echo e(route('procedure-instructions')); ?>">Hướng dẫn</a>
                         </li>
                         <li>
                            <a href="#">Gia hạn visa</a>
                        </li>
                        <li>
                            <a href="#">Thẻ tạm trú</a>
                        </li>
                        <li>
                            <a href="#">Dịch vụ</a>
                        </li>
                        <li>
                            <a href="#">Theo dõi tiến độ</a>
                        </li>
                        <li>
                            <a href="#">Liên hệ</a>
                        </li>
                    </ul>
                </div>
                <div class="search-header-2">
                 <form action="">
                    <div class="input-group search-box">
                        <input type="text" class="form-control" placeholder="Recipient's username" aria-label="Recipient's username" aria-describedby="basic-addon2">
                        <div class="input-group-append">
                            <!-- <span >@example.com</span> -->
                            <button class="input-group-text" id="basic-addon2">Search</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
</header>
<div class="banner">
    <img src="<?php echo e(asset($banner->content)); ?>" alt="">
</div><?php /**PATH C:\xampp\htdocs\evisa\resources\views/font-end/partials/header.blade.php ENDPATH**/ ?>