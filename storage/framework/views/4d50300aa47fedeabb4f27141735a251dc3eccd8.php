<!DOCTYPE html>
<html>
<head>
    <?php echo $__env->make('admin.partials.head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php echo $__env->yieldContent('css'); ?>
</head>
<body>
    <div id="wrapper">
        <?php echo $__env->make('admin.partials.left-side-bar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <div id="page-wrapper" class="gray-bg">
            <div class="row border-bottom">
                <?php echo $__env->make('admin.partials.top-bar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            </div>
            <?php echo $__env->make('admin.partials.breadcrumb-form', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            
            <div class="wrapper wrapper-content">
                <div class="row animated fadeInRight">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Thông tin chi tiết</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <?php echo $__env->make('admin.partials.error-list', ['flag'=>$flag], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                            <form class="form-horizontal" role="form" 
                            action="<?php echo e($route_action); ?>" method="POST">
                            <?php echo method_field($method); ?>
                            <?php echo csrf_field(); ?>
                            <!--Panel -->
                            <div class="panel-panel-default">
                                <div id="info" class="panel-collapse-collapse">
                                    <div class="panel-body-c">
                                    <?php echo $__env->yieldContent('content'); ?>
                                    </div>   
                                </div>
                            </div>
                            <!-- -->
                            <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <button class="btn btn-white" type ="reset">Reset</button>
                                <button class="btn btn-primary" type="submit"><?php echo e($btn_submit); ?></button>
                            </div>                              
                            </form>                         
                        </div>
                    </div>
                </div>      
            </div>
            
        </div>              
    </div>
</body>
<?php echo $__env->make('admin.partials.scripts', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->yieldContent('js'); ?>
</html>
<?php /**PATH C:\xampp\htdocs\evisa\resources\views/admin/layouts/form-default.blade.php ENDPATH**/ ?>