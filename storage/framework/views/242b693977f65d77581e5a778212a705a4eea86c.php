<?php $__env->startSection('content'); ?>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-10">
		<h2><?php echo e($title); ?></h2>
		<ol class="breadcrumb">
			<li>
				<a href="#">Bảng tin</a>
			</li>
			<li>
				<a><?php echo e($title); ?></a>
			</li>
		</ol>
	</div>
	<div class="col-lg-2">
	</div>
</div>
<div class="wrapper wrapper-content">
	<iframe src="<?php echo e(url('../filemanager/dialog.php?type=0&field_id=thumb_0')); ?>" style="width: 100%; height: 800px; overflow: hidden; border: none;"></iframe>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('admin.layouts.default', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\evisa\resources\views/admin/gallery.blade.php ENDPATH**/ ?>