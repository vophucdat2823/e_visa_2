@extends('admin.layouts.default')

@section('title')
Quản lý giao diện người dùng
@parent
@stop

@section('css')
<link href="{{asset('assets/css/plugins/iCheck/custom.css')}}" rel="stylesheet">
@stop

{{-- Page content --}}
@section('content')
{{-- Breadcrumb --}}
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-4">
        <h2>Quản lý giao diện người dùng</h2>
        <ol class="breadcrumb">
	        <li>
	            <a href="index.html">Home</a>
	        </li>
	        <li class="active">
	            <strong>Quản lý giao diện người dùng</strong>
	        </li>
        </ol>
    </div>
    <div class="col-sm-8">
        <div class="title-action">
          
        </div>
    </div>
</div>
{{-- END Breadcrumb --}}
{{-- Main content --}}
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox float-e-margins">
				{{-- Header table --}}
				<div class="ibox-title">
					<h5>Quản lý giao diện người dùng</h5>
					<div class="ibox-tools">
						<a class="collapse-link">
							<i class="fa fa-chevron-up"></i>
						</a>						
					</div>
				</div>
				{{-- END Header table --}}
				<div class="ibox-content">
					{{-- <div class="row">
                        <div class="col-sm-5 m-b-xs">
                            <div class="btn-group">
	                            <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle">Tác vụ <span class="caret"></span></button>
	                            <ul class="dropdown-menu">
	                                <li><a href="#" class="font-bold">Xóa</a></li>
	                                <li><a href="#" class="font-bold">Chỉnh sửa</a></li>
	                            </ul>
                        	</div>
                        </div>
                    </div> --}}
					{{-- Data table --}}
					<div class="table-responsive">
							@if(session('success-theme'))
								<div class="alert alert-success">
									<strong>{{session('success-theme')}}</strong>
								</div>
							@endif
						<table class="table table-striped">
							<thead>
								<tr>
									<th>Tên </th>
									<th>Nội dung </th>
									<th>Tác vụ</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td colspan="3" style="background-color:#1ab394;color: #fff;" class="text-center">HEADER</td>
								</tr>
								@foreach ($theme_header as $item)
									<tr>
										<td>{{$item->name}}</td>
										<td>
											@if ($item->keyword==='banner' || $item->keyword==='logo')
												<img width="200" src="{{asset($item->content)}}" alt="">
											@else
												{{$item->content}}
											@endif
										</td>
										<td>
											<a href="{{url('admin/theme-option/edit?id='.$item->id)}}" class="btn btn-warning btn-circle"><i class="fa fa-edit"></i></a>
										</td>
									</tr>
								@endforeach	
								<tr>
									<td colspan="3" style="background-color:#1ab394;color: #fff;" class="text-center">FOOTER</td>
								</tr>
								@foreach ($theme_footer as $item)
									<tr>
										<td>{{$item->name}}</td>
										<td>{{$item->content}}</td>
										<td>
											<a href="{{url('admin/theme-option/edit?id='.$item->id)}}" class="btn btn-warning btn-circle"><i class="fa fa-edit"></i></a>
										</td>
									</tr>
								@endforeach
								<tr>
									<td colspan="3" style="background-color:#1ab394;color: #fff;" class="text-center">LINK ICON</td>
								</tr>
								@foreach ($link_icon as $item)
									<tr>
										<td> <img width="50" src="{{asset($item->name)}}" alt=""></td>
										<td>{{$item->content}}</td>
										<td>
											<a href="{{url('admin/theme-option/edit?id='.$item->id)}}" class="btn btn-warning btn-circle"><i class="fa fa-edit"></i></a>
										</td>
									</tr>
								@endforeach
								<tr>
									<td colspan="3" style="background-color:#1ab394;color: #fff;" class="text-center">QUICK LINK</td>
								</tr>
								@foreach ($quick_link as $item)
									<tr>
										<td>{{$item->name}}</td>
										<td>{{$item->content}}</td>
										<td>
											<a href="{{url('admin/theme-option/edit?id='.$item->id)}}" class="btn btn-warning btn-circle"><i class="fa fa-edit"></i></a>
										</td>
									</tr>
								@endforeach															
							</tbody>
						</table>
					</div>
				{{-- END Data table --}}
				</div>
			</div>
		</div>
	</div>
</div>
{{-- END Main content --}}
@stop
@section('script')
	
<!-- iCheck -->
<script src="{{asset('assets/js/plugins/iCheck/icheck.min.js')}}"></script>

<!-- Peity -->
<script src="{{asset('assets/js/demo/peity-demo.js')}}"></script>
<script>
    $(document).ready(function(){
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
         });
    });
</script>
@stop