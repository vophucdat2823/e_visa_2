@extends('admin.layouts.default')

@section('title')
Chỉnh sửa giao diện
@parent
@stop

@section('css')
<link href="{{asset('assets/css/plugins/datapicker/datepicker3.css')}}" rel="stylesheet">
@stop

{{-- Page content --}}
@section('content')
{{-- Breadcrumb --}}
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-4">
        <h2>Chỉnh sửa giao diện</h2>
        <ol class="breadcrumb">
	        <li>
	            <a href="{{route('home')}}">Home</a>
	        </li>
	        <li>
	            {{-- <a href="{{route('list-user')}}">Danh sách người dùng</a> --}}
	        </li>
	        <li class="active">
	            <strong>Chỉnh sửa giao diện</strong>
	        </li>
        </ol>
    </div>
</div>
{{-- END Breadcrumb --}}

{{-- START Main Content --}}
<div class="wrapper wrapper-content">
	<div class="row animated fadeInRight">
		<div class="ibox float-e-margins">
			<div class="ibox-title">
	            <h5>Thông tin</h5>
	            <div class="ibox-tools">
		            <a class="collapse-link">
		                <i class="fa fa-chevron-up"></i>
		            </a>
	            </div>
	        </div>
	        <div class="ibox-content">
					@if(session('error-theme'))
						<div class="alert alert-danger">
							<strong>{{session('error-theme')}}</strong>
						</div>
					@endif
				<form method="post" action="{{route('edit-theme')}}" class="form-horizontal" enctype="multipart/form-data">
						{{ csrf_field() }}
		        	<div class="form-group"><label class="col-sm-2 control-label">Tên </label>
					<div class="col-sm-10"><input type="text" class="form-control" name="name" disabled id="name" value="{{$theme->name}}"></div>
					</div>
					@if ($theme->keyword==='logo' || $theme->keyword==='banner' )
						<div class="hr-line-dashed"></div>
						<div class="form-group">
							<label class="col-sm-2 control-label"></label>

							<div class="col-sm-10">
								<img src="{{asset($theme->content)}}" alt="imagereview" id="imagereview" style="width:50%;height:50%">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Hình ảnh</label>

							<div class="col-sm-10">
								<input type="file" required class="form-control" name="image" id="image">
							</div>
						</div>
					@else
						<div class="hr-line-dashed"></div>
							<div class="form-group"><label class="col-sm-2 control-label">Nội dung</label>
							<div class="col-sm-10"><input type="text" required class="form-control" name="content" id="content" placeholder="nội dung" value="{{$theme->content}}"></div>
						</div>
					@endif
	              
					<input type="hidden" name="id" value={{$theme->id}}>
					<input type="hidden" name="keyword" value={{$theme->keyword}}>
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            <button class="btn btn-primary" type="submit">Lưu lại</button>
                        </div>
                    </div>
            	</form>
	        </div>
		</div>
	</div>		
</div>
{{-- END Main Content --}}

@stop
{{-- Page content --}}

@section('script')
<script src="{{asset('assets/js/plugins/datapicker/bootstrap-datepicker.js')}}"></script>
<script>
    $(document).ready(function(){
		function readURL(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();
				
				reader.onload = function(e) {
					$('#imagereview').attr('src', e.target.result);
				}
				
				reader.readAsDataURL(input.files[0]);
			}
		}

		$("#image").change(function() {
			console.log("XXX");
			readURL(this);
		});
    });
</script>	
@stop