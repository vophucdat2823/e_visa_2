@extends('admin.layouts.default')

{{-- Page content --}}
@section('content')
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-10">
		<h2>{{$title}}</h2>
		<ol class="breadcrumb">
			<li>
				<a href="#">Bảng tin</a>
			</li>
			<li>
				<a>{{$title}}</a>
			</li>
		</ol>
	</div>
	<div class="col-lg-2">
	</div>
</div>
<div class="wrapper wrapper-content">
	<iframe src="{{url('../filemanager/dialog.php?type=0&field_id=thumb_0')}}" style="width: 100%; height: 800px; overflow: hidden; border: none;"></iframe>
</div>
@stop
