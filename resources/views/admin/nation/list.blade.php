@extends('admin.layouts.table-default')

@section('css')
@endsection

@section('content')
@if(session('error-nation'))
<div class="alert alert-danger">
	<strong>{{session('error-nation')}}</strong>
</div>
@endif
@if(session('success-nation'))
<div class="alert alert-success">
	<strong>{{session('success-nation')}}</strong>
</div>
@endif

<div class="table-responsive">
<table class="table table-striped table-bordered table-hover dataTables-example">
<thead style="color: #000;font-weight: bold;">
	<tr>
		<td class="text-center">ID</td>
		<td class="text-center">Tên</td>
		<td class="text-center">Value</td>
		<td class="text-center">Type</td>
		<td class="text-center">Trạng thái</td>
		<td class="text-center"></td>
	</tr>
</thead>
<tbody>
	@foreach($objs as $obj)
	<tr>
		<td class="text-center">{{$obj->id}}</td>
		<td class="text-center">{{$obj->name}}</td>
		<td class="text-center">{{$obj->value}}</td>
		<td class="text-center">
			@if($obj->type_evisa == 1 && $obj->type_visa_arrival !=1)
				<span class="badge badge-success">Evisa</span>
			@elseif($obj->type_evisa != 1 && $obj->type_visa_arrival ==1)
				<span class="badge badge-success">Visa cửa khẩu</span>
			@else
				<span class="badge badge-success">Cả 2 loại</span> 
			@endif
		</td>
		<td class="text-center">
			@if($obj->status == 1)
				<span class="badge badge-success">Sử dụng</span>
			@else
				<span class="badge badge-danger">Không sử dụng</span> 
			@endif
		</td>
		<td class="text-center">
			<form action="{{route('nation.edit', ['id' => $obj->id])}}">
				<button class="btn btn-warning btn-sm" type="submit">
						Sửa
				<span class="fa fa-edit" data-fa-transform="shrink-3"></span>
				</button>
				<button class="btn btn-danger btn-sm delete-button" 
					data-action ="{{ route('nation.destroy',$obj->id) }}" type="button">
						Xóa
				<span class="fa fa-trash" data-fa-transform="shrink-3"></span>
				</button>	
			</form>
		</td>
	</tr>
	@endforeach
</tbody>
</table>
</div>
@endsection

@section('js')
@endsection