@if(session('error-nation'))
<div class="alert alert-danger">
	<strong>{{session('error-nation')}}</strong>
</div>
@endif
@if(session('success-nation'))
<div class="alert alert-success">
	<strong>{{session('success-nation')}}</strong>
</div>
@endif
<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
	<label class="col-sm-2 control-label">Tên (*) </label>
	<div class="col-sm-4">
		<input type="text" class="form-control" name="name" id="name" 
		value="{{isset($obj) ? $obj->name : old('name')}}" required>
	</div>
</div>
<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
	<label class="col-sm-2 control-label">Loại (*) </label>
	<div class="col-sm-2">
		<div class="radio">
			<label>
			  <input type="radio" name="typevisa"  value="1" 
				@if(isset($obj) && $obj->type_evisa==1 && $obj->type_visa_arrival !=1)
					checked
				@endif
			  >
			  Evisa
			</label>
		</div>
	</div>
	<div class="col-sm-2">
		<div class="radio">
			<label>
			  <input type="radio" name="typevisa"  value="2"
			  	@if(isset($obj) && $obj->type_visa_arrival==1 && $obj->type_evisa != 1)
					checked
				@endif
			  >
			  Visa cửa khẩu
			</label>
		</div>
	</div>
	<div class="col-sm-2">
		<div class="radio">
			<label>
			  <input type="radio" name="typevisa" value="3" 
			  	@if(isset($obj) && $obj->type_visa_arrival == 1 && $obj->type_evisa==1)
					checked
				@elseif(!isset($obj) )
					checked
				@endif
			  >
			 Cả 2 loại
			</label>
		</div>
	</div>
</div>
<div class="form-group {{ $errors->has('value') ? 'has-error' : '' }}">
	<label class="col-sm-2 control-label">Giá trị (*) </label>
	<div class="col-sm-4">
		<input type="number" min="0" class="form-control" name="value" id="value" 
		value="{{isset($obj) ? $obj->value : old('value')}}" required>
	</div>
</div>
@include('admin.partials.status')