 <li class="nav-header">
    <div class="dropdown profile-element"> 
        <span>
            <img alt="image" class="img-circle" src="{{asset('assets/img/user-default.png')}}" />
        </span>
        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">{{Auth::user()->name}}</strong>
                <span class="clear"> <span class="block m-t-xs"> <small class="font-bold">{{Auth::user()->email}}</small>
                </span> <span class="text-muted text-xs block">Admin <b class="caret"></b></span> </span> </a>
                <ul class="dropdown-menu animated fadeInRight m-t-xs">
                    <li><a href="#">Profile</a></li>
                    <li><a href="#">Contacts</a></li>
                    <li><a href="#">Mailbox</a></li>
                    <li class="divider"></li>
                    <li><a href="{{ route('logout') }}"  onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">Logout</a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                       @csrf
                   </form>

               </li>
           </ul>
       </div>
       <div class="logo-element">
        DNM
    </div>
</li>
