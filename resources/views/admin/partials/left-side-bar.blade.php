<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            @include('admin.partials.navbar-left.nav-header')
            <li class={{$flag == "bangtin" ? "active" : ""}}>
                <a href="#"><i class="fa fa-th-large"></i> <span class="nav-label">Bảng tin</span></span></a>
            </li>
            <li class={{$flag == "customer" ? "active" : ""}}>
                <a href="{{route('customer.index')}}"><i class="fa fa-picture-o"></i> <span class="nav-label">Danh sách yêu cầu</span></a>
            </li>
            <li class={{$flag == "nation" ? "active" : ""}}>
                <a href="{{route('nation.index')}}"><i class="fa fa-picture-o"></i> <span class="nav-label">Nationality</span></a>
            </li>
            <li class={{$flag == "purpose" ? "active" : ""}}>
                <a href="{{route('purpose.index')}}"><i class="fa fa-picture-o"></i> <span class="nav-label">Purpose of Visa</span></a>
            </li>
            <li class={{$flag == "type" ? "active" : ""}}>
                <a href="{{route('type.index')}}"><i class="fa fa-picture-o"></i> <span class="nav-label">Type of Visa</span></a>
            </li>
            <li class={{$flag == "processing" ? "active" : ""}}>
                <a href="{{route('processing.index')}}"><i class="fa fa-picture-o"></i> <span class="nav-label">Processing Time</span></a>
            </li>
            <li class={{$flag == "files" ? "active" : ""}}>
                <a href="{{route('gallery')}}"><i class="fa fa-picture-o"></i> <span class="nav-label">Files</span></a>
            </li>
            <li class={{$flag == "header" ? "active" : ""}}>
                    <a href="{{route('indexHeader')}}"><i class="fa fa-picture-o"></i> <span class="nav-label">Theme Option</span></a>
            </li>
            <li class="landing_link">
                <a target="_blank" href="{{config('website.domain')}}"><i class="fa fa-star"></i> <span class="nav-label">Website</span> <span class="label label-warning pull-right"></span></a>
            </li>
        </ul>
    </div>
</nav>