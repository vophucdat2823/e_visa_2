<div class="form-group">
	<label class="col-sm-2 control-label">Trạng thái (*)</label>
	<div class="col-md-4">
		@if(!isset($obj))
			<select class="form-control m-b" name="status">
				<option value="1" {{old('status') == "1" ? "selected" : ""}}>Sử dụng</option>
				<option value="0" {{old('status') == "0" ? "selected" : ""}}>Không sử dụng</option>
			</select> 
		@else
			<select class="form-control m-b" name="status">
				<option value="1" {{$obj->status == "1" ? "selected" : ""}}>Sử dụng</option>
				<option value="0" {{$obj->status == "0" ? "selected" : ""}}>Không sử dụng</option>
			</select>
		@endif                                       
	</div>
</div>