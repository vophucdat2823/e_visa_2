{{-- Breadcrumb --}}
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-4">
        <h2>{{$title}}</h2>
        <ol class="breadcrumb">
	        <li>
	            <a href="#">Bảng tin</a>
	        </li>
	        <li>
	            <a href="{{$route_list}}">Danh sách</a>
	        </li>
	        <li class="active">
	            <strong>{{$title}}</strong>
	        </li>
        </ol>
    </div>
</div>
{{-- END Breadcrumb --}}