<div id="statusModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title" id="status-modal-title"></h4>
			</div>
			<div class="modal-body">	
				<form id="modal-form-status" action="" method="POST">
				  <input type="hidden" value="" id="type_status" name="type_status">
		          @csrf
		          <button class="btn hoi" type="submit">Có</button>
		          <button type="button" class="close" data-dismiss="modal">Không
				 </button>
		        </form>			
			</div>
		</div>
	</div>
</div>