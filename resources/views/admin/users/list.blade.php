@extends('admin.layouts.default')

@section('title')
Danh sách người dùng
@parent
@stop

@section('css')
<link href="{{asset('assets/css/plugins/iCheck/custom.css')}}" rel="stylesheet">
@stop

{{-- Page content --}}
@section('content')
{{-- Breadcrumb --}}
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-4">
        <h2>Danh sách người dùng</h2>
        <ol class="breadcrumb">
	        <li>
	            <a href="index.html">Home</a>
	        </li>
	        <li class="active">
	            <strong>Danh sách người dùng</strong>
	        </li>
        </ol>
    </div>
    <div class="col-sm-8">
        <div class="title-action">
            <a href="{{route('create-user')}}" class="btn btn-primary">Thêm mới</a>
        </div>
    </div>
</div>
{{-- END Breadcrumb --}}
{{-- Main content --}}
<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox float-e-margins">
				{{-- Header table --}}
				<div class="ibox-title">
					<h5>Danh sách người dùng</h5>
					<div class="ibox-tools">
						<a class="collapse-link">
							<i class="fa fa-chevron-up"></i>
						</a>						
					</div>
				</div>
				{{-- END Header table --}}
				<div class="ibox-content">
					<div class="row">
                        <div class="col-sm-5 m-b-xs">
                            <div class="btn-group">
	                            <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle">Tác vụ <span class="caret"></span></button>
	                            <ul class="dropdown-menu">
	                                <li><a href="#" class="font-bold">Xóa</a></li>
	                                <li><a href="#" class="font-bold">Chỉnh sửa</a></li>
	                            </ul>
                        	</div>
                        </div>
                    </div>
					{{-- Data table --}}
					<div class="table-responsive">
						<table class="table table-striped">
							<thead>
								<tr>
									<th><input type="checkbox"  checked class="i-checks" namse="input[]"></th>
									<th>Tên </th>
									<th>Email </th>
									<th>Ngày tạo</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td><input type="checkbox"  checked class="i-checks" name="input[]"></td>
									<td>Project<small>This is example of project</small></td>
									<td><span class="pie">0.52/1.561</span></td>
									<td>Jul 14, 2013</td>
									<td><a href="#" class="btn btn-success btn-circle"><i class="fa fa-eye"></i></a>
										<a href="#" class="btn btn-warning btn-circle"><i class="fa fa-edit"></i></a>
										<a href="#" class="btn btn-danger btn-circle"><i class="fa fa-trash"></i></a>
									</td>
								</tr>
								<tr>
									<td><input type="checkbox" class="i-checks" name="input[]"></td>
									<td>Alpha project</td>
									<td><span class="pie">6,9</span></td>
									<td>Jul 16, 2013</td>
									<td><a href="#" class="btn btn-success btn-circle"><i class="fa fa-eye"></i></a>
										<a href="#" class="btn btn-warning btn-circle"><i class="fa fa-edit"></i></a>
										<a href="#" class="btn btn-danger btn-circle"><i class="fa fa-trash"></i></a>
									</td>
								</tr>
								<tr>
									<td><input type="checkbox" class="i-checks" name="input[]"></td>
									<td>Betha project</td>
									<td><span class="pie">3,1</span></td>
									<td>Jul 18, 2013</td>
									<td><a href="#" class="btn btn-success btn-circle"><i class="fa fa-eye"></i></a>
										<a href="#" class="btn btn-warning btn-circle"><i class="fa fa-edit"></i></a>
										<a href="#" class="btn btn-danger btn-circle"><i class="fa fa-trash"></i></a>
									</td>
								</tr>
								<tr>
									<td><input type="checkbox" class="i-checks" name="input[]"></td>
									<td>Gamma project</td>
									<td><span class="pie">4,9</span></td>
									<td>Jul 22, 2013</td>
									<td><a href="#" class="btn btn-success btn-circle"><i class="fa fa-eye"></i></a>
										<a href="#" class="btn btn-warning btn-circle"><i class="fa fa-edit"></i></a>
										<a href="#" class="btn btn-danger btn-circle"><i class="fa fa-trash"></i></a>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				{{-- END Data table --}}
				</div>
			</div>
		</div>
	</div>
</div>
{{-- END Main content --}}
@stop
@section('script')
	
<!-- iCheck -->
<script src="{{asset('assets/js/plugins/iCheck/icheck.min.js')}}"></script>

<!-- Peity -->
<script src="{{asset('assets/js/demo/peity-demo.js')}}"></script>
<script>
    $(document).ready(function(){
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
         });
    });
</script>
@stop