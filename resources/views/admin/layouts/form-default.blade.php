<!DOCTYPE html>
<html>
<head>
    @include('admin.partials.head')
    @yield('css')
</head>
<body>
    <div id="wrapper">
        @include('admin.partials.left-side-bar')
        <div id="page-wrapper" class="gray-bg">
            <div class="row border-bottom">
                @include('admin.partials.top-bar')
            </div>
            @include('admin.partials.breadcrumb-form')
            {{-- START Main Content --}}
            <div class="wrapper wrapper-content">
                <div class="row animated fadeInRight">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Thông tin chi tiết</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            @include('admin.partials.error-list', ['flag'=>$flag])
                            <form class="form-horizontal" role="form" 
                            action="{{$route_action}}" method="POST">
                            @method($method)
                            @csrf
                            <!--Panel -->
                            <div class="panel-panel-default">
                                <div id="info" class="panel-collapse-collapse">
                                    <div class="panel-body-c">
                                    @yield('content')
                                    </div>   
                                </div>
                            </div>
                            <!-- -->
                            <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <button class="btn btn-white" type ="reset">Reset</button>
                                <button class="btn btn-primary" type="submit">{{$btn_submit}}</button>
                            </div>                              
                            </form>                         
                        </div>
                    </div>
                </div>      
            </div>
            {{-- END Main Content --}}
        </div>              
    </div>
</body>
@include('admin.partials.scripts')
@yield('js')
</html>
