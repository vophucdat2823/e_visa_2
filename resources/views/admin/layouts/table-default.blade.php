<!DOCTYPE html>
<html>
<head>
    @include('admin.partials.head')
    <link href="{{asset('assets/css/plugins/dataTables/datatables.min.css')}}" rel="stylesheet">
    @yield('css')
</head>
<body>
    <div id="wrapper">
        @include('admin.partials.left-side-bar')
        <div id="page-wrapper" class="gray-bg">
            <div class="row border-bottom">
                @include('admin.partials.top-bar')
            </div>
            @include('admin.partials.breadcrumb-table')
            {{-- START Main Content --}}
            <div class="wrapper wrapper-content">
                <div class="row animated fadeInRight">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>{{$title}}</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            @yield('content')
                            @include('admin.partials.modals.modal-confirm-delete')
                            @include('admin.partials.modals.modal-confirm-status')
                        </div>
                    </div>
                </div>      
            </div>
            {{-- END Main Content --}}
        </div>              
    </div>
</body>
@include('admin.partials.scripts')
<script src="{{asset('assets/js/plugins/dataTables/datatables.min.js')}}"></script>
<script src="{{asset('js/evisa.js')}}"></script>
<script>
    $(document).ready(function(){
        $('.dataTables-example').DataTable({
            pageLength: 10,
            responsive: true,
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
            { extend: 'copy'},
            {extend: 'csv'},
            {extend: 'excel', title: 'ExampleFile'},
            {extend: 'pdf', title: 'ExampleFile'},
            {extend: 'print',
            customize: function (win){
                $(win.document.body).addClass('white-bg');
                $(win.document.body).css('font-size', '10px');
                $(win.document.body).find('table')
                .addClass('compact')
                .css('font-size', 'inherit');
            }
        }
        ]
    });
    });
</script>
@yield('script')
</html>
