@extends('admin.layouts.table-default')

@section('css')
@endsection

@section('content')
@if(session('error-customer'))
<div class="alert alert-danger">
	<strong>{{session('error-customer')}}</strong>
</div>
@endif
@if(session('success-customer'))
<div class="alert alert-success">
	<strong>{{session('success-customer')}}</strong>
</div>
@endif

<div class="table-responsive">
<table class="table table-striped table-bordered table-hover dataTables-example">
<thead style="color: #000;font-weight: bold;">
	<tr>
		<td class="text-center">#</td>
		<td class="text-center">Họ tên</td>
		<td class="text-center">Ngày đăng ký</td>
		<td class="text-center">Tình trạng</td>
		<td class="text-center">Dịch vụ</td>
		<td class="text-center">Tác vụ</td>
	</tr>
</thead>
<tbody>
	@foreach($objs as $key=>$obj)
	<tr>
		<td class="text-center">{{$key+1}}</td>
		<td class="text-center">{{$obj->name}}</td>
		<td class="text-center">{{$obj->created_at}}</td>
		<td class="text-center">
			@if($obj->status == 0)
				<span class="badge badge-warning">Chưa phản hồi</span>
			@elseif($obj->status == 1)
				<span class="badge badge-success">Đã phản hồi</span> 
			@elseif($obj->status == 2)
				<span class="badge badge-danger">Từ chối</span> 
			@endif
		</td>
		<td class="text-center">
			@if($obj->type === "evisa")
				<span class="badge badge-default">Visa điện tử</span>
			@elseif($obj->type === "visa_arrival")
				<span class="badge badge-default">Visa cửa khẩu</span> 
			@endif
		</td>
		<td class="text-center">
			<button class="btn btn-success btn-sm "  type="button">
					<a style="color:#fff" href="{{route('detailOrder',$obj->id) }}">Chi tiết</a>
					
			</button>
			<button class="btn btn-danger btn-sm status-button" data-action ="{{route('update-status',$obj->id) }}" data-type="2" type="button">
					Xóa
				<span class="fa fa-trash" data-fa-transform="shrink-3"></span>
			</button>
			{{-- @if($obj->status == 0)
				<button class="btn btn-success btn-sm status-button" 
					data-action ="{{route('update-status',$obj->id) }}" data-type="1" type="button">
						Tán thành
				<span class="fa fa-edit" data-fa-transform="shrink-3"></span>
				</button>
				<button class="btn btn-danger btn-sm status-button" 
					data-action ="{{route('update-status',$obj->id) }}" data-type="2" type="button">
						Xóa
				<span class="fa fa-trash" data-fa-transform="shrink-3"></span>
				</button>
			@endif --}}
		</td>
		
	</tr>
	@endforeach
</tbody>
</table>
</div>
@endsection

@section('js')
@endsection