@if(session('error-processing'))
<div class="alert alert-danger">
	<strong>{{session('error-processing')}}</strong>
</div>
@endif
@if(session('success-processing'))
<div class="alert alert-success">
	<strong>{{session('success-processing')}}</strong>
</div>
@endif

@include('admin.partials.status')