@extends('admin.layouts.default')

@section('title')
Chi tiết đơn hàng
@parent
@stop

@section('css')
<link href="{{asset('assets/css/plugins/datapicker/datepicker3.css')}}" rel="stylesheet">
@stop

{{-- Page content --}}
@section('content')
{{-- Breadcrumb --}}
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-4">
        <h2>Chi tiết đơn hàng</h2>
        <ol class="breadcrumb">
	        <li>
	            <a href="{{route('home')}}">Home</a>
	        </li>
	        <li>
	            {{-- <a href="{{route('list-user')}}">Danh sách người dùng</a> --}}
	        </li>
	        <li class="active">
	            <strong>Chi tiết đơn hàng</strong>
	        </li>
        </ol>
    </div>
</div>
{{-- END Breadcrumb --}}

{{-- START Main Content --}}
<div class="wrapper wrapper-content">
	<div class="row animated fadeInRight">
		<div class="ibox float-e-margins">
			<div class="ibox-title">
	            <h5>Thông tin</h5>
	            <div class="ibox-tools">
		            <a class="collapse-link">
		                <i class="fa fa-chevron-up"></i>
		            </a>
	            </div>
	        </div>
	        <div class="ibox-content">
					<form  class="form-horizontal">
							<div class="form-group">
								<div class="col-12 text-center" style="background:#1cc09f;color:#fff;padding:3px;font-size:17px">
										<label class=" control-label">FORM </label>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Nationality : </label>
								<div class="col-sm-10">
									<input type="text" class="form-control" name="address" disabled id="address" value="{{$order['nation']->name}}">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Purpose of Visa: </label>
								<div class="col-sm-10">
									<input type="text" class="form-control" name="purpose" disabled id="purpose" value="{{$order['purpose']->name}}">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Type of Visa : </label>
								<div class="col-sm-10">
									<input type="text" class="form-control" name="type" disabled id="type" value="{{$order['type']->name}}">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Processing Time: </label>
								<div class="col-sm-10">
									<input type="text" class="form-control" name="processing" disabled id="processing" value="{{$order['processing']->name}}">
								</div>
							</div>
							<div class="form-group">
								<div class="col-12 text-center" style="background:#1cc09f;color:#fff;padding:3px;font-size:17px">
										<label class=" control-label">APPLICANT DETAILS </label>
								</div>
							</div>
							@if ($order['fileprofile'] )
								<div class="hr-line-dashed"></div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Ảnh cá nhân</label>
		
									<div class="col-sm-10">
										<img src="{{asset($order['fileprofile'])}}" alt="imagereview" id="imagereview" style="width:50%;height:50%">
									</div>
								</div>
							@endif
							@if ($order['filepassport'] )
								<div class="hr-line-dashed"></div>
								<div class="form-group">
									<label class="col-sm-2 control-label">Ảnh hộ chiếu</label>
		
									<div class="col-sm-10">
										<img src="{{asset($order['filepassport'])}}" alt="imagereview" id="imagereview" style="width:50%;height:50%">
									</div>
								</div>
							@endif
						<div class="form-group">
							<label class="col-sm-2 control-label">Tên: </label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="name" disabled id="name" value="{{$order['name']}}">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Passport number: </label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="pass_number" disabled id="pass_number" value="{{$order['pass_number']}}">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Cửa khẩu: </label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="cuakhau" disabled id="cuakhau" value="{{$order['cuakhau']}}">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Ngày vào VN: </label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="date_in" disabled id="date_in" value="{{$order['date_in']}}">
							</div>
						</div>
						<div class="form-group">
							<div class="col-12 text-center" style="background:#1cc09f;color:#fff;padding:3px;font-size:17px">
									<label class=" control-label">CONTACT INFORMATION </label>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Số điện thoại: </label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="phone" disabled id="phone" value="{{$order['phone']}}">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Email: </label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="email" disabled id="email" value="{{$order['email']}}">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Ghi chú: </label>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="note" disabled id="note" value="{{$order['note']}}">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Hình thức thanh toán : </label>
							@if ($order['type_tt']==1)
								<div class="col-sm-10">
									<input type="text" class="form-control" name="type_tt" disabled id="type_tt" value="Thanh toán tại văn phòng">
								</div>
							@elseif ($order['type_tt']==2)
								<div class="col-sm-10">
									<input type="text" class="form-control" name="type_tt" disabled id="type_tt" value="Paypal">
								</div>
							@elseif ($order['type_tt']==3)
								<div class="col-sm-10">
									<input type="text" class="form-control" name="type_tt" disabled id="type_tt" value="Chuyển khoản">
								</div>
							@endif
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Trạng thái đơn hàng: </label>
							@if ($order['status']==0)
								<div class="col-sm-10">
									<input type="text" class="form-control" name="status" disabled id="status" value="Chưa phản hồi">
								</div>
							@elseif($order['status']==1)
								<div class="col-sm-10">
									<input type="text" class="form-control" name="status" disabled id="status" value="Đã phản hồi">
								</div>
							@endif
						</div>
						
					</div>
				
            	</form>
				
	        </div>
		</div>
	</div>		
</div>
{{-- END Main Content --}}

@stop
{{-- Page content --}}

@section('script')
<script src="{{asset('assets/js/plugins/datapicker/bootstrap-datepicker.js')}}"></script>
<script>
    $(document).ready(function(){
		function readURL(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();
				
				reader.onload = function(e) {
					$('#imagereview').attr('src', e.target.result);
				}
				
				reader.readAsDataURL(input.files[0]);
			}
		}

		$("#image").change(function() {
			console.log("XXX");
			readURL(this);
		});
    });
</script>	
@stop