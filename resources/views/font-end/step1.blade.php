<!DOCTYPE html>

<html lang="en">

@include('font-end.partials.head')

<body>

 @include('font-end.partials.header')

 <div class="main">

    <div class="container">

        <div class="row">

            <div class="col-lg-12">

                <div class="bg-main-white">

                    <div class="beadcrumb">

                        <ul>

                            <li>

                                <a href="{{URL::to('/')}}">Home</a><span>&nbsp; /</span>

                            </li>

                            {{-- <li>

                                <a href="#">Apply E-visa</a><span>&nbsp; /</span>

                            </li> --}}

                            <li>

                                <a href="#" class="active">Step 1: Hướng dẫn</a>

                            </li>

                        </ul>

                    </div>

                    <div class="content-step-1">

                        <h3>E-VISA APPLY ONLINE</h3>

                        <p>

                            Dành Cho Người Nước Ngoài Đang Ở Nước Ngoài Trực Tiếp Đề Nghị Cấp Thị Thực Điện Tử. Visa điện tử / e visa :  visa không quá 30 ngày/ 1 lần -  mục đích : thăm thân, du lịch, công tác, học tập,  thi cử ... Nếu Quý khách ngoài mục này xin vào Visa on Arrival  

                        </p>

                        <h5 style="color: #B01803;">Bước 1: Nhập thông tin đề nghị cấp thị thực điện tử:</h5>

                        <p>Nạp ảnh trang nhân thân hộ chiếu và ảnh mặt chân dung (mặt nhìn thẳng, không đeo kính)</p>

                        <img src="{{asset('e-visa/images/image-step-1.jpg')}}" alt="">

                        <h5 style="color: #B01803;">Bước 2: Nộp phí cấp thị thực điện tử  (Xem biểu phí 2019)</h5>

                        <h5 style="color: #B01803;">Bước 3: Sử dụng Mã hồ sơ điện tử để kiểm tra kết quả, nếu được chấp thuận, thực hiện in thị thực điện tử để nhập/xuất cảnh Việt Nam.</h5>

                        <p style="font-weight: 600; font-size: 17px;">* Lưu ý: Nếu hồ sơ của ông/bà thiếu hoặc có thông tin sai, không xác định được thì hồ sơ của ông/bà sẽ không được chấp nhận.</p>

                        <div class="form-accept">

                            <div class="checkbox">

                                <label><input id="chk_confirm" type="checkbox" value="1" style="margin-right: 15px;">Xác nhận đã đọc kỹ hướng dẫn và chuẩn bị đầy đủ hồ sơ</label>

                            </div>

                            <button style="background: #17396B;" type="button"

                                onclick="window.location.href='{{route('home')}}'">QUAY LẠI

                            </button>
                            @if ($check==="evisa")
                                <button id="btnNext"  type="button" style=""  
                                    onclick="window.location.href='{{route('evisa-step-2')}}'">TIẾP TỤC

                                </button>
                            @else
                                <button id="btnNext"  type="button" style=""  
                                    onclick="window.location.href='{{route('evisa-arrival-step-2')}}'">TIẾP TỤC

                                </button>
                            @endif
                           

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>
<script>
     var check=<?php echo json_encode( isset($check)?$check:Session::get('check')); ?>;
     var service_support =0;
    var arrType=[];
    var arrNation=[];
    var arrPupose=[];
    var arrProcess=[];
</script>
@include('font-end.partials.footer')

@include('font-end.partials.scripts')
<script>
   
    $(document).ready(function(){
        $("#btnNext").prop('disabled', true);
        $("#btnNext").css("background", "#dedede");
        $("#chk_confirm").change(function(){
            // alert($('#chk_confirm').prop('checked'));
            var check1 = $('#chk_confirm').prop('checked');
            if(check1 == false){
                $("#btnNext").prop('disabled', true);
                $("#btnNext").css("background", "#dedede");
            }else{
                $("#btnNext").prop('disabled', false);
                $("#btnNext").css("background", "#B01803");
            }
        });
    });
</script>    

</body>

</html>