<!DOCTYPE html>
<html lang="en">
@include('font-end.partials.head')
<body>
@include('font-end.partials.header')
<div class="main">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="bg-main-white procedure-instructions">
                    <h6>1. HƯỚNG DẪN THỦ TỤC</h6>
                    <ul class="tabs-instructions-1">
                        <li onclick="instructions('instructions-1')">
                            <i class="fas fa-caret-right"></i><a href="javascript:void(0)"> CẤP HỘ CHIẾU PHỔ THÔNG</a>
                            <div class="content-tabs-instructions-1" id="instructions-1" style="display: block;">
                                <h6>1. Hồ sơ bao gồm:</h6>
                                <p>
                                    - Khai 01 tờ khai đề nghị cấp hộ chiếu theo mẫu quy định (X01). <br/>
                                    - 02 ảnh cỡ 4x6, nền trắng, mặt nhìn thẳng, đầu để trần, không đeo kính màu (chụp ảnh tại cơ  sở chụp ảnh được cơ quan Quản lý xuất nhập cảnh cho phép)<br/>
                                    - Giấy chứng minh nhân dân hoặc Thẻ căn cước công dân còn giá trị (xuất trình khi nộp hồ sơ để cơ quan Quản lý xuất nhập cảnh kiểm tra, đối chiếu).<br/>
                                    - Sổ tạm trú (nếu nộp hồ sơ tại nơi tạm trú).<br/>
                                </p>
                                <h6>2. Lệ phí: 200.000 đ (Hai trăm nghìn đồng).</h6>
                            </div>
                        </li>
                        <li onclick="instructions('instructions-2')">
                            <i class="fas fa-caret-right"></i><a href="javascript:void(0)"> CẤP LẠI HỘ CHIẾU DO BỊ HƯ HỎNG, BỊ MẤT</a>
                            <div class="content-tabs-instructions-1 fadeInDown" id="instructions-2" style="display: none;">
                            </div>
                        </li>
                        <li onclick="instructions('instructions-3')">
                            <i class="fas fa-caret-right"></i><a href="javascript:void(0)"> CẤP CHUNG HỘ CHIẾU PHỔ THÔNG CHO CÔNG DÂN VÀ TRẺ EM DƯỚI 9 TUỔI</a>
                            <div class="content-tabs-instructions-1 fadeInDown" id="instructions-3" style="display: none;">
                            </div>
                        </li>
                        <li onclick="instructions('instructions-4')">
                            <i class="fas fa-caret-right"></i><a href="javascript:void(0)"> CẤP RIÊNG HỘ CHIẾU PHỔ THÔNG CHO TRẺ EM DƯỚI 14 TUỔI</a>
                            <div class="content-tabs-instructions-1 fadeInDown" id="instructions-4" style="display: none;">
                            </div>
                        </li>
                        <li onclick="instructions('instructions-5')">
                            <i class="fas fa-caret-right"></i><a href="javascript:void(0)"> SỬA ĐỔI, BỔ SUNG THÔNG TIN TRONG HỘ CHIẾU</a>
                            <div class="content-tabs-instructions-1 fadeInDown" id="instructions-5" style="display: none;">
                            </div>
                        </li>
                    </ul>
                    <h6>2. NƠI NỘP HỒ SƠ</h6>
                    <p>
                        - Người đề nghị cấp hộ chiếu lần đầu, đề nghị cấp lại hộ chiếu do hết hạn trực tiếp nộp hồ sơ tại Phòng Quản lý xuất nhập cảnh Công an tỉnh, thành phố trực thuộc Trung ương nơi thường trú hoặc tạm trú.
                    </p>
                    <p>
                        - Người đề nghị cấp hộ chiếu do sắp hết hạn, do hư hỏng hoặc bị mất, đề nghị sửa đổi, bổ sung hộ chiếu nộp hồ sơ tại Phòng Quản lý xuất nhập cảnh Công an tỉnh, thành phố trực thuộc Trung ương nơi thường trú, tạm trú hoặc Cục Quản lý xuất nhập cảnh, Bộ Công an.
                    </p>
                    <p>
                        Lưu ý: <br/>
                        - Người khai thông tin phải chịu trách nhiệm về tính xác thực của nội dung đã khai. Đối với trường hợp gửi hồ sơ đề nghị cấp lại hộ chiếu qua đường Bưu điện, trường hợp trẻ em dưới 14 tuổi đề nghị cấp hộ chiếu hoặc trường hợp ủy thác cho cơ quan, tổ chức, doanh nghiệp có tư cách pháp nhân nộp hồ sơ đề nghị cấp hộ chiếu, sau khi khai tờ khai điện tử người đề nghị phải in tờ khai và dán ảnh vào tờ khai để lấy xác nhận và giáp lai ảnh của Công an phường, xã, thị trấn nơi thường trú hoặc tạm trú hoặc Thủ trưởng cơ quan, tổ chức, doanh nghiệp được ủy thác. Đối với trường hợp trực tiếp nộp hồ sơ, người đề nghị cấp hộ chiếu có thể chụp ảnh bằng cách liên hệ với cơ sở chụp ảnh được cơ quan Quản lý xuất nhập cảnh cho phép.
                    </p>
                    <p>
                        - Trường hợp đề nghị cấp chung 02 trẻ em vào hộ chiếu của cha hoặc mẹ, sau khi nhập thông tin phải in 02 tờ khai (mỗi tờ tương ứng với thông tin của 01 trẻ em) để lấy xác nhận của Công an phường, xã, thị trấn nơi trẻ em thường trú hoặc tạm trú; trong trường hợp này, hộ chiếu của cha, mẹ cấp chung với con dưới 9 tuổi có thời hạn 05 năm.
                    </p>
                    <h6>3. TRÌNH TỰ THỰC HIỆN KHAI TỜ KHAI ĐIỆN TỬ ĐỀ NGHỊ CẤP HỘ CHIẾU</h6>
                    <p>Bước 1: Nhập đầy đủ thông tin tờ khai đề nghị cấp hộ chiếu.</p>
                    <p>Bước 2: In tờ khai đề nghị cấp hộ chiếu (tại nhà hoặc tại nơi nộp hồ sơ).</p>
                    <p>Bước 3: Kiểm tra, chỉnh sửa thông tin đã khai (nếu có nhu cầu) tại đây</p>
                    <p>Bước 4: Đặt lịch hẹn nộp hồ sơ (nếu có nhu cầu).</p>
                    <p>Bước 5: Đến nộp hồ sơ tại cơ quan Quản lý xuất nhập cảnh.</p>
                    <div class="btn-pay text-center" style="margin-top: 40px">
                        <button style="background: #276E44;" type="button" 
                            onclick="window.location.href='{{route('home')}}'">QUAY LẠI
                        </button>
                        <button style="background: #276E44;">TIẾP TỤC</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('font-end.partials.footer')
@include('font-end.partials.scripts')
</body>
</html>