<!DOCTYPE html>
<html lang="en">
@include('font-end.partials.head')
<body>
 @include('font-end.partials.header')
 <div class="container">
    <div class="row">
        <div class="col-sm-12 title-content">
            <h2>Chọn dịch vụ Visa</h2>
            <span>Nhanh chóng - Tin cậy - Chính xác</span>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 col-sm-6 col-lg-4 col-xs-12 col-xs-12">
            <div class="box-service-home">
                <div class="icon-box-service">
                    <img src="{{asset('e-visa/images/service-1.png')}}" alt="">
                </div>
                <h3>Visa điện tử</h3>
                <p>Visa không quá 30 ngày/1 lần - mục địch: thăm thân, du lịch, công tác, học tập</p>
                <a href="{{ url('/step-2-evisa.html') }}" class="btn-apply-service">Apply >></a>
                <a href="#" class="btn-findout-service">Tìm hiểu >></a>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-lg-4 col-xs-12">
            <div class="box-service-home">
                <div class="icon-box-service">
                    <img src="{{asset('e-visa/images/service-2.png')}}" alt="">
                </div>
                <h3>Visa cửa khẩu</h3>
                <p>Visa không quá 90 ngày - 1 lần/ nhiều lần - mục đích: du lịch</p>
                <a href="{{ url('/step-2-evisa-arrival.html') }}" class="btn-apply-service">Apply >></a>
                <a href="#" class="btn-findout-service">Tìm hiểu >></a>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-lg-4 col-xs-12">
            <div class="box-service-home">
                <div class="icon-box-service">
                    <img src="{{asset('e-visa/images/service-3.png')}}" alt="">
                </div>
                <h3>Dịch vụ gia tăng</h3>
                <p>Dịch vụ mở rộng: Vé máy bay, vé tàu, xe đón tiễn sân bay, khách sạn, gia hạn visa</p>
                <a href="#" class="btn-apply-service">Apply >></a>
                <a href="#" class="btn-findout-service">Tìm hiểu >></a>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>        
</div>
<form class="form-horizontal" role="form" 
action="{{route('step-1')}}" method="POST">
@csrf
<div class="box-form-bg-sliver">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-lg-3 col-xs-12">
                <label>Nationality</label>
                <select class="form-control" id="nation" name="nation"> 
                    @foreach($nations as $nation)
                    <option data-value="{{$nation->value}}" value="{{$nation->id}}">{{$nation->name}}</option>
                    @endforeach
                    <option data-value="orther" value="0">Orther Nationality</option>
                </select>
            </div>
            <div class="col-md-6 col-sm-6 col-lg-3 col-xs-12">
                <label>Purpose Visa</label>
                <select class="form-control" id="purpose" name="purpose">
                    @foreach($purposes as $purpose)
                        <option data-value="{{$purpose->value}}" value="{{$purpose->id}}">{{$purpose->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-6 col-sm-6 col-lg-3 col-xs-12">
                <label>Type of Visa</label>
                <select class="form-control" id="type" name="type">         
                    @foreach($types as $type)
                    <option data-value="{{$type->value}}" value="{{$type->id}}">{{$type->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-6 col-sm-6 col-lg-3 col-xs-12">
                <label>Processing Time</label>
                <select class="form-control"  id="process" name="process">
                    @foreach($process as $pro)        
                    <option data-value="{{$pro->value}}" value="{{$pro->id}}">{{$pro->name}}</option>
                    @endforeach
                </select>
                <input type="hidden" name="check" id="check" value="evisa" />
            </div>
        </div>
        <div class="row justify-content-md-center">
            <div class="col-md-auto box-price col-lg-8 col-md-12 col-sm-12 col-xs-12">
                <table class="table" id="table_fee">
                    <tbody id="tbody_fee">
                        <tr>
                            <td class="font-weight-bold border-white">NATIONALITY</td>
                            <td class="font-weight-bold border-white text-right" id="pre_nation">X $0 = $0</td>
                        </tr>
                        <tr>
                            <td class="font-weight-bold border-white">PURPOSE VISA</td>
                            <td class="font-weight-bold border-white text-right" id="pre_purpose">X $0 = $0</td>
                        </tr>
                        <tr>
                            <td class="font-weight-bold border-white">TYPE OF VISA</td>
                            <td class="font-weight-bold border-white text-right" id="pre_type">X $0 = $0</td>
                        </tr>
                        <tr>
                            <td class="font-weight-bold border-white">PROCESSING FEE</td>
                            <td class="font-weight-bold border-white text-right" id="pre_pro">X $0 = $0</td>
                        </tr>
                        <tr class="total-price">
                            <td>PROCESSING FEE</td>
                            <td class="text-right" id="pre_sum">X $0 = $0</td>
                        </tr>
                    </tbody>
                </table>
                <button type="submit">Apply</button>
            </div>
        </div>
    </div>
</div>
</form>
<div class="step-home">        
    <div class="container">
        <div class="row">
            <div class="col-sm-12 title-content">
                <h2>04 Bước đơn giản</h2>
            </div>
            <div class="col-md-6 col-sm-6 col-lg-3 col-xs-12">
                <div class="box-step">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 60 55.21"><defs><style>.cls-1{fill:#b01803;}</style></defs><title>chose visa</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1" d="M48.24,4.21H24a1.17,1.17,0,0,0,0,2.34H48.24a1.17,1.17,0,0,0,0-2.34Z"/><path class="cls-1" d="M12.1,4.55a1.17,1.17,0,1,0,.34.83A1.18,1.18,0,0,0,12.1,4.55Z"/><path class="cls-1" d="M7.8,4.55a1.17,1.17,0,1,0,.34.83A1.18,1.18,0,0,0,7.8,4.55Z"/><path class="cls-1" d="M16.4,4.55a1.17,1.17,0,1,0,.34.83A1.18,1.18,0,0,0,16.4,4.55Z"/><path class="cls-1" d="M59.66,48.72l-9.21-9.21,1.44-1.44h0l1.44-1.44,4.55,4.55a1.17,1.17,0,0,0,1.65-1.65l-5.38-5.38a1.17,1.17,0,0,0-1.65,0l-1.45,1.45-.7-.7A13.69,13.69,0,0,0,30,16.61H7.49a1.17,1.17,0,0,0-1.17,1.17V34.15a1.17,1.17,0,0,0,1.17,1.17H29.81a13.67,13.67,0,0,0,18.9,1.21l.7.7L48,38.68a1.17,1.17,0,0,0,0,1.65l4.9,4.9V50.3a2.58,2.58,0,0,1-2.57,2.57H4.91A2.58,2.58,0,0,1,2.34,50.3V10.76H52.87V16a1.17,1.17,0,1,0,2.34,0V4.91A4.92,4.92,0,0,0,50.3,0H4.91A4.92,4.92,0,0,0,0,4.91V50.3a4.92,4.92,0,0,0,4.91,4.91H50.3a4.92,4.92,0,0,0,4.91-4.91V47.58l2.8,2.8a1.17,1.17,0,1,0,1.65-1.65ZM26.2,26.42l-1.47-2.85a1.17,1.17,0,0,0-2.08,1.08L25.4,30a1.16,1.16,0,0,0,1.53.52A13.63,13.63,0,0,0,28.08,33H8.66v-14H28.19a13.61,13.61,0,0,0-2,7.11Q26.2,26.24,26.2,26.42Zm13.69,11A11.37,11.37,0,0,1,28.54,26.06c0-.09,0-.18,0-.27L30.69,30a1.17,1.17,0,0,0,1,.63h0a1.17,1.17,0,0,0,1-.66l2.55-5.31a1.17,1.17,0,0,0-2.11-1L31.69,26.8,30,23.57a1.17,1.17,0,0,0-1-.63A11.36,11.36,0,1,1,39.9,37.42ZM2.34,4.91A2.58,2.58,0,0,1,4.91,2.34H50.3a2.58,2.58,0,0,1,2.57,2.57V8.42H2.34Z"/><path class="cls-1" d="M21.38,23.06a1.17,1.17,0,0,0-1.56.55L18.29,26.8l-1.67-3.22a1.17,1.17,0,0,0-2.09,0L13,26.8l-1.67-3.22a1.17,1.17,0,1,0-2.08,1.08L12,30a1.17,1.17,0,0,0,2.09,0l1.53-3.19L17.29,30a1.17,1.17,0,0,0,1,.63h0a1.17,1.17,0,0,0,1-.66l2.55-5.31A1.17,1.17,0,0,0,21.38,23.06Z"/><path class="cls-1" d="M48.19,23.06a1.17,1.17,0,0,0-1.56.55L45.09,26.8l-1.67-3.22a1.17,1.17,0,0,0-2.09,0L39.8,26.8l-1.67-3.22a1.17,1.17,0,0,0-2.08,1.08L38.8,30a1.17,1.17,0,0,0,1,.63h0a1.17,1.17,0,0,0,1-.66l1.53-3.19L44.09,30a1.17,1.17,0,0,0,1,.63h0a1.17,1.17,0,0,0,1-.66l2.55-5.31A1.17,1.17,0,0,0,48.19,23.06Z"/><path class="cls-1" d="M8.31,44.56a1.17,1.17,0,1,0,.34.83A1.17,1.17,0,0,0,8.31,44.56Z"/><path class="cls-1" d="M34.28,44.21H11.5a1.17,1.17,0,1,0,0,2.34H34.28a1.17,1.17,0,0,0,0-2.34Z"/><path class="cls-1" d="M29.94,38.6H23a1.17,1.17,0,1,0,0,2.34h6.9a1.17,1.17,0,0,0,0-2.34Z"/><path class="cls-1" d="M17.79,38.6H7.49a1.17,1.17,0,1,0,0,2.34h10.3a1.17,1.17,0,1,0,0-2.34Z"/></g></g></svg>
                    <h3>Lựa chọn Visa</h3>
                    <p>E-Visa hay Visa on Arrival theo mục đích sử dụng của bạn</p><br/>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-lg-3 col-xs-12">
                <div class="box-step">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50.32 60"><defs><style>.cls-1{fill:#b01803;}</style></defs><title>fill info</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1" d="M44.52,12.18V3.87H30.87a4.84,4.84,0,0,0-9.48,0H0V54.19H5.81V60H50.32V18Zm-1.94.8L47,17.42H42.58Zm0-2.74L42,9.68H31V5.81H42.58Zm-40.65,42V5.81H21.29V9.68H5.81V52.26Zm5.81,5.81V11.61H21.29v2.9a2.9,2.9,0,1,0,5.81,0V5.81H25.16v8.71a1,1,0,1,1-1.94,0V4.84a2.9,2.9,0,1,1,5.81,0V17.42H31V11.61h9.68v7.74h7.74V58.06Z"/><path class="cls-1" d="M28.06,48.39A14.52,14.52,0,1,0,13.55,33.87,14.53,14.53,0,0,0,28.06,48.39Zm0-1.94A12.54,12.54,0,0,1,19,42.58H37.12A12.54,12.54,0,0,1,28.06,46.45Zm-1.45-14A2.87,2.87,0,0,1,25.16,30V28.06a2.91,2.91,0,0,1,.94-2.14,2.88,2.88,0,0,1,2.22-.75A3,3,0,0,1,31,28.27V30a2.87,2.87,0,0,1-1.45,2.5l-.48.28v2.79L33.7,36.9a2.92,2.92,0,0,1,2.11,2.79v1H20.32v-1a2.92,2.92,0,0,1,2.11-2.79l4.67-1.33V32.78Zm1.45-11.21A12.57,12.57,0,0,1,37.74,41.9v-2.2A4.86,4.86,0,0,0,34.23,35L31,34.11v-.26A4.84,4.84,0,0,0,32.9,30V28.27a5,5,0,0,0-4.41-5,4.84,4.84,0,0,0-5.27,4.82V30a4.84,4.84,0,0,0,1.94,3.85v.26L21.9,35a4.86,4.86,0,0,0-3.51,4.65v2.2a12.57,12.57,0,0,1,9.68-20.61Z"/><path class="cls-1" d="M9.68,50.32H46.45v1.94H9.68Z"/><path class="cls-1" d="M9.68,54.19h32.9v1.94H9.68Z"/><path class="cls-1" d="M44.52,54.19h1.94v1.94H44.52Z"/></g></g></svg>
                    <h3>Hoàn thành thông tin </h3>
                    <p>Điền thông tin theo các hướng dẫn của hệ thống để hoàn thành thủ tục</p>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-lg-3 col-xs-12">
                <div class="box-step">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 78.59 60"><defs><style>.cls-1{fill:#b01803;}</style></defs><title>price</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1" d="M0,39.52c1.45,0,2.91.05,4.36.06H17.88V41.8a3.33,3.33,0,0,1-.5.07H.45A3.38,3.38,0,0,1,0,41.82Z"/><path class="cls-1" d="M48.61,0a30,30,0,1,1-30,29.93A30.09,30.09,0,0,1,48.61,0ZM76.28,30a27.7,27.7,0,1,0-27.7,27.68A27.77,27.77,0,0,0,76.28,30Z"/><path class="cls-1" d="M3.34,34.68V32.47H17v2.21Z"/><path class="cls-1" d="M17,25.3v2.2H6.62V25.3Z"/><path class="cls-1" d="M17.89,18.18v2.19h-8V18.18Z"/><path class="cls-1" d="M48.57,56.09A26.09,26.09,0,1,1,74.67,30,26.15,26.15,0,0,1,48.57,56.09Zm.1-49.88A23.79,23.79,0,1,0,72.35,30.07,23.82,23.82,0,0,0,48.67,6.21Z"/><path class="cls-1" d="M34.91,42.77,38.34,36a22.75,22.75,0,0,0,8.76,3.76,5.12,5.12,0,0,0,.08-.63c0-1.74,0-3.48,0-5.22,0-.5-.18-.67-.65-.84a59.47,59.47,0,0,1-5.83-2.29c-3.05-1.5-4.4-4.09-4.38-7.48A10.3,10.3,0,0,1,43.8,13.08c1.07-.33,2.17-.56,3.37-.86V7.47h3.56v4.84A26.63,26.63,0,0,1,61.25,16l-3.43,6.49a20.36,20.36,0,0,0-7.08-3c0,1,0,1.94,0,2.88s0,2,0,3.07c0,.47.16.63.61.78a61.32,61.32,0,0,1,5.89,2.13c3.59,1.63,5.15,4.62,5,8.47-.16,5.21-3,8.51-8.06,9.65a24.59,24.59,0,0,1-2.72.44c-.53.06-.76.21-.74.79,0,1.51,0,3,0,4.53a2,2,0,0,1-.06.33H47.19c0-1.4,0-2.8,0-4.2s0-1.39-1.38-1.58a26.26,26.26,0,0,1-10.34-3.59C35.3,43,35.14,42.93,34.91,42.77Zm15.88-2.9a13.53,13.53,0,0,0,1.46-.36A2.15,2.15,0,0,0,53.74,37a3.37,3.37,0,0,0-3-2.44ZM47.14,19.45a2.57,2.57,0,0,0-2.64,2.3c-.12,1.59.8,2.68,2.64,3.09Z"/></g></g></svg>
                    <h3>Nộp lệ phí</h3>
                    <p>Nộp lệ phí thông qua các cổng thanh toán trực tuyến uy tín và an toàn</p>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-lg-3 col-xs-12">
                <div class="box-step">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50.41 60"><defs><style>.cls-1{fill:#b01803;}</style></defs><title>result</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1" d="M50.4,15.45s0,0,0,0,0,0,0-.06,0,0,0,0l0-.06s0,0,0,0a.37.37,0,0,0,0-.1l0,0,0-.06,0,0,0,0L39.1.48h0L39,.39s0,0,0,0a.33.33,0,0,0-.1-.09h0L38.79.2l0,0-.09,0h0l-.09,0h0l-.09,0H1.23A1.23,1.23,0,0,0,0,1.23V58.77A1.23,1.23,0,0,0,1.23,60h48a1.23,1.23,0,0,0,1.23-1.23V15.57h0v0S50.4,15.48,50.4,15.45Zm-3.71-1H39.37l0-9.57ZM48,57.55H2.46V2.47H36.89l0,13.2a1.23,1.23,0,0,0,1.23,1.23H48Z"/><path class="cls-1" d="M7.66,22.51a1.23,1.23,0,0,0,1.23,1.23H41.62a1.23,1.23,0,1,0,0-2.46H8.89A1.23,1.23,0,0,0,7.66,22.51Z"/><path class="cls-1" d="M8.89,16.24H30.27a1.23,1.23,0,0,0,0-2.46H8.89a1.23,1.23,0,1,0,0,2.46Z"/><path class="cls-1" d="M8.89,31.24H41.62a1.23,1.23,0,1,0,0-2.46H8.89a1.23,1.23,0,1,0,0,2.46Z"/><path class="cls-1" d="M30.27,38.74a1.23,1.23,0,0,0,0-2.46H8.89a1.23,1.23,0,1,0,0,2.46Z"/><path class="cls-1" d="M40.81,40.32,30.45,49.46l-3-4a1.23,1.23,0,1,0-2,1.48L29.29,52a1.22,1.22,0,0,0,.86.49h.12a1.21,1.21,0,0,0,.81-.32l11.35-10a1.23,1.23,0,0,0-1.62-1.84Z"/></g></g></svg>
                    <h3>Trả kết quả</h3>
                    <p>Sau 3-5 ngày làm việc kết quả của Khách hàng sẽ được thông báo</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-sm-6 col-lg-4 col-xs-12 box-share">
                <div class="title-box">
                    <h3>Chia sẽ kiến thức</h3>
                </div>
                <div class="row">
                    <div class="col-12 mt-3">
                        <div class="card card-blog-home">
                            <div class="card-horizontal">
                                <div class="img-square-wrapper">
                                    <img class="" src="{{asset('e-visa/images/blog-1.jpg')}}" alt="Card image cap">
                                </div>
                                <div class="card-body">
                                    <h4 class="card-title">
                                        <a href="#">Night Market Opens For Street Vendors In Hcm City</a>
                                    </h4>
                                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 mt-3">
                        <div class="card card-blog-home">
                            <div class="card-horizontal">
                                <div class="img-square-wrapper">
                                    <img class="" src="{{asset('e-visa/images/blog-1.jpg')}}" alt="Card image cap">
                                </div>
                                <div class="card-body">
                                    <h4 class="card-title">
                                        <a href="#">Night Market Opens For Street Vendors In Hcm City</a>
                                    </h4>
                                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-lg-4 col-xs-12 box-support">
                <div class="title-box">
                    <h3>Hỗ trợ trực tuyến</h3>
                </div>
                <div class="card-support mt-3">
                    <h4>Hỗ trợ dịch vụ 24/7</h4>
                    <p><span style="color:#FFC200 ">Phone:</span> +84(024).39263484</p>
                    <p><span style="color:#FFC200 ">Fax:</span> +84(024)39260570</p>
                    <p><span style="color:#FFC200 ">Mail:</span> e-visa.top@gmail.com</p>
                </div>
            </div>
            <div class="col-md-12 col-sm-12 col-lg-4 col-xs-12 box-question">
                <div class="title-box">
                    <h3>Câu hỏi thường gặp</h3>
                </div>
                <div class="card-question mt-3">
                    <ul>
                        <li>
                            <a href="#"><i class="far fa-question-circle"></i> Có thể nhận được visa nếu chuyến bay bị trì hoãn?</a>
                        </li>
                        <li>
                            <a href="#"><i class="far fa-question-circle"></i> Visa Việt Nam khi đến chương trình hợp pháp?</a>
                        </li>
                        <li>
                            <a href="#"><i class="far fa-question-circle"></i> Có thể nhận được visa nếu chuyến bay bị trì hoãn?</a>
                        </li>
                        <li>
                            <a href="#"><i class="far fa-question-circle"></i> Visa Việt Nam khi đến chương trình hợp pháp?</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-lg-4 col-xs-12 box-share commitment">
                <p>Chúng tôi chỉ tính phí cho ứng dụng thành công. Hoàn tiền 100% nếu visa bị từ chối.</p>
            </div>
            <div class="col-md-12 col-sm-12 col-lg-4 col-xs-12 box-support partner">
                <ul>
                    <li>
                        <a href="#">
                            <img src="{{asset('e-visa/images/paypal.png')}}" alt="">
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img src="{{asset('e-visa/images/visa.png')}}" alt="">
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img src="{{asset('e-visa/images/master-card.png')}}" alt="">
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img src="{{asset('e-visa/images/jcb.png')}}" alt="">
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <img src="{{asset('e-visa/images/america-express.png')}}" alt="">
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-md-12 col-sm-12 col-lg-4 col-xs-12 box-question btn-more">
                <button>Xem thêm các câu hỏi thường gặp</button>
            </div>
        </div>
    </div>
</div>
@php
    $arrType=json_encode($types);
    $arrNation=json_encode($nations);
    $arrPupose=json_encode($purposes);
    $arrProcess=json_encode($process);
@endphp
<script>
    var arrType=<?php echo $arrType; ?>;
    var arrNation=<?php echo $arrNation; ?>;
    var arrPupose=<?php echo $arrPupose; ?>;
    var arrProcess=<?php echo $arrProcess; ?>;
    var service_support =0;
</script>
@include('font-end.partials.footer')
@include('font-end.partials.scripts')
</body>
</html>