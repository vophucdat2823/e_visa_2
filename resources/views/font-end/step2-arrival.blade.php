
<!DOCTYPE html>
<html lang="en">
@include('font-end.partials.head')
<body>

    @include('font-end.partials.header')


    <div class="main">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="bg-main-white">
                        <div class="beadcrumb">
                            <ul>
                                <li>
                                    <a href="{{URL::to('/')}}">Home</a><span>&nbsp; /</span>
                                </li>
                                {{-- <li>
                                    <a href="#">Apply E-visa</a><span>&nbsp; /</span>
                                </li> --}}
                                <li>
                                    <a href="#" class="active">Step 2: Điền form thông tin</a>
                                </li>
                            </ul>
                        </div>
                        <div class="content-step-2">
                            <h3>E-VISA APPLY ONLINE</h3>
                            <div class="menu-tabs">
                                <ul>
                                    <li class="{{$step == 2 ? 'active' : ''}}" onclick="openCity('step1')">
                                        <a href="javascript:void(0)" data-toggle="tab" href="#step1">1. ADD APPLICANT FILL FORM</a>
                                    </li>
                                    <li class="{{$step == 3 ? 'active' : ''}}" onclick="openCity('step2')">
                                        <a href="javascript:void(0)" data-toggle="tab" href="#step2">2. SUDMIT TO PAY</a>
                                    </li>
                                    <li class="{{$step == 4 ? 'active' : ''}}" onclick="openCity('step3')">
                                        <a href="javascript:void(0)" data-toggle="tab" href="#step3">3. COMPLETE</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 tabs-left">
                                    <div class="content-tabs">
                                        <div id="step1" class="container-tabs" {{$step == 2 ? 'style=display:block;' : 'style=display:none;'}} >
                                            <div class="row">
                                                <div class="col-lg-7">
                                                    <div class="form-info">
                                                        <form class="form-horizontal" role="form" enctype="multipart/form-data"
                                                        action="{{route('create-visa')}}" method="POST">
                                                        @csrf
                                                        <h4><img src="{{asset('e-visa/images/icon-form.png')}}" alt=""> FILL ONLINE FORM</h4>
                                                        <input type="hidden" name="visa_id" value="{{isset($visa_new) ? $visa_new->id : '' }}">
                                                        <input type="hidden" name="check" value="{{isset($check) ? $check : Session::get('check') }}">
                                                        <div class="form-group row">
                                                            <label for="inputPassword" class="col-sm-5 col-form-label">Nationality <span style="color: #FF1D25">(as in Passport *)</span></label>
                                                            <div class="col-sm-7">
                                                                <select class="form-control" id="nation_id" name="nation_id" required>            
                                                                    @foreach($nations as $nation)
                                                                    @if(isset($visa_new))
                                                                    <option data-value="{{$nation->value}}" value="{{$nation->id}}" 
                                                                        {{$visa_new->nation_id == $nation->id ? 'selected' : ''}}>
                                                                        {{$nation->name}}
                                                                    </option>
                                                                    @else
                                                                    <option data-value="{{$nation->value}}" value="{{$nation->id}}" 
                                                                        {{Session::get('nation') == $nation->id ? 'selected' : ''}}>
                                                                        {{$nation->name}}
                                                                    </option>
                                                                    @endif
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="inputPassword" class="col-sm-5 col-form-label">Purpose of Visa <span style="color: #FF1D25">(*)</span></label>
                                                            <div class="col-sm-7">
                                                                <select class="form-control" id="purpose_id" name="purpose_id" required>                 
                                                                    @foreach($purposes as $purpose)
                                                                       @if(isset($visa_new))
                                                                        <option data-value="{{$purpose->value}}" value="{{$purpose->id}}" 
                                                                            {{$visa_new->purpose_id == $purpose->id ? 'selected' : ''}}>
                                                                            {{$purpose->name}}
                                                                        </option>
                                                                        @else
                                                                        <option data-value="{{$purpose->value}}" value="{{$purpose->id}}" 
                                                                            {{Session::get('purpose') == $purpose->id ? 'selected' : ''}}>
                                                                            {{$purpose->name}}
                                                                        </option>
                                                                        @endif
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="inputPassword" class="col-sm-5 col-form-label">Type of Visa <span style="color: #FF1D25">(*)</span></label>
                                                            <div class="col-sm-7">
                                                                <select class="form-control" id="type_id" name="type_id" required>                 
                                                                    @foreach($types as $type)
                                                                        @if(isset($visa_new))
                                                                            <option data-value="{{$type->value}}" value="{{$type->id}}" 
                                                                                {{$visa_new->type_id == $type->id ? 'selected' : ''}}>
                                                                                {{$type->name}}
                                                                            </option>
                                                                        @else
                                                                            <option data-value="{{$type->value}}" value="{{$type->id}}" 
                                                                                {{Session::get('type') == $type->id ? 'selected' : ''}}>
                                                                                {{$type->name}}
                                                                            </option>
                                                                        @endif
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label for="inputPassword" class="col-sm-5 col-form-label">Processing Time <span style="color: #FF1D25">(*)</span></label>
                                                            <div class="col-sm-7">
                                                             <select class="form-control"  id="processing_id" name="processing_id" required>                      
                                                                @foreach($pross as $pro)        
                                                                    @if(isset($visa_new))
                                                                        <option data-value="{{$pro->value}}" value="{{$pro->id}}" 
                                                                            {{$visa_new->processing_id == $pro->id ? 'selected' : ''}}>
                                                                            {{$pro->name}}
                                                                        </option>
                                                                        @else
                                                                        <option data-value="{{$pro->value}}" value="{{$pro->id}}" 
                                                                            {{Session::get('processing') == $pro->id ? 'selected' : ''}}>
                                                                            {{$pro->name}}
                                                                        </option>
                                                                    @endif
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <h4><img src="{{asset('e-visa/images/icon-form.png')}}" alt=""> Service</h4>
                                                    <div class="form-group row">
                                                        <label class="col-sm-5 col-form-label">Dịch vụ hổ trợ làm thủ tục nhanh:</label>
                                                        <div class="col-sm-7">
                                                            <input type="checkbox" name="service_support" id="service_support" value="1"> Sử dụng dịch vụ<br>
                                                        </div>
                                                    </div>
                                                    <h4><img src="{{asset('e-visa/images/icon-form.png')}}" alt=""> APPLICANT DETAILS</h4>
                                                    {{-- <div class="form-group row">
                                                        <label class="col-sm-5 col-form-label">Tải ảnh cá nhân</label>
                                                        <div class="col-sm-7">
                                                            <input type="file" name="fileToUpload" id="fileToUpload">
                                                        </div>
                                                    </div> --}}
                                                    <div class="form-group row">
                                                            <label class="col-sm-5 col-form-label">Tải ảnh hộ chiếu</label>
                                                            <div class="col-sm-7">
                                                                <input type="file" name="filepassport" id="filepassport">
                                                            </div>
                                                        </div>
                                                    <div class="form-group row">
                                                        <label class="col-sm-5 col-form-label">Name <span style="color: #FF1D25">(Fullname *)</span></label>
                                                        <div class="col-sm-7">
                                                            <input type="text" name="name" id="name"  class="form-control" placeholder="fill your full name" value="{{isset($visa_new) ? $visa_new->name : old('name')}}" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-sm-5 col-form-label">Passport number <span style="color: #FF1D25">(*)</span></label>
                                                        <div class="col-sm-7">
                                                            <input type="text" name ="pass_number" id="pass_number" class="form-control" placeholder="fill your passport number" value="{{isset($visa_new) ? $visa_new->pass_number : old('pass_number')}}" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="inputPassword" class="col-sm-5 col-form-label">Cửa khẩu vào Việt Nam <span style="color: #FF1D25">(*)</span></label>
                                                        <div class="col-sm-7">
                                                            <select class="form-control" name="cuakhau" id="cuakhau" required>
                                                                <option label=" "></option>
                                                                <option value="Nội Bài"  {{isset($visa_new) ? ($visa_new->cuakhau == "Nội Bài" ? 'selected' : '') : '' }}>Nội Bài</option>
                                                                <option value="Sao Vàng" {{isset($visa_new) ? ($visa_new->cuakhau == "Sao Vàng" ? 'selected' : '') : '' }}>Sao Vàng</option>
                                                                <option value="Móng Cái" {{isset($visa_new) ? ($visa_new->cuakhau == "Móng Cái" ? 'selected' : '') : '' }}>Móng Cái</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-sm-5 col-form-label">Ngày dự định vào Việt Nam <span style="color: #FF1D25">(*)</span></label>
                                                        <div class="col-sm-7">
                                                            <input type="date" name="date_in" id="date_in" class="form-control" placeholder="01/01/2019" value="{{isset($visa_new) ? $visa_new->date_in : old('date_in')}}" required>
                                                        </div>
                                                    </div>
                                                    <h4><img src="{{asset('e-visa/images/icon-form.png')}}" alt="">CONTACT INFORMATION</h4>
                                                    <div class="form-group row">
                                                        <label class="col-sm-5 col-form-label">Số điện thoại <span style="color: #FF1D25">(*)</span></label>
                                                        <div class="col-sm-4">
                                                            @php
                                                                echo countrySelector("VN", "my-country", "my-country", "form-control",countryArray());
                                                            @endphp
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <input type="text" name="phone" id="phone" class="form-control" placeholder="0984 062 393" value="{{isset($visa_new) ? $visa_new->phone : old('phone')}}" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-sm-5 col-form-label">Email <span style="color: #FF1D25">(*)</span></label>
                                                        <div class="col-sm-7">
                                                            <input type="text" name="email" id="email" class="form-control" placeholder="Email" value="{{isset($visa_new) ? $visa_new->email : old('email')}}" required>
                                                        </div>
                                                    </div>
                                                    {{-- <div class="form-group row">
                                                        <label for="inputPassword" class="col-sm-5 col-form-label">Địa chỉ lưu trú</label>
                                                        <div class="col-sm-7">
                                                            <select class="form-control" name="address" id="address">
                                                                <option label=" "></option>
                                                                <option value="Nội Bài"  {{isset($visa_new) ? ($visa_new->address == "Nội Bài" ? 'selected' : '') : '' }}>Nội Bài</option>
                                                                <option value="Sao Vàng" {{isset($visa_new) ? ($visa_new->address == "Sao Vàng" ? 'selected' : '') : '' }}>Sao Vàng</option>
                                                                <option value="Móng Cái" {{isset($visa_new) ? ($visa_new->address == "Móng Cái" ? 'selected' : '') : '' }}>Móng Cái</option>
                                                            </select>
                                                        </div>
                                                    </div> --}}
                                                    <div class="form-group row">
                                                        <label for="inputPassword" class="col-sm-5 col-form-label">Yêu cầu đặc biệt</label>
                                                        <div class="col-sm-7">
                                                            <textarea class="form-control" rows="5" id="note" name="note" value="{{isset($visa_new) ? $visa_new->note : old('note')}}"></textarea>
                                                        </div>
                                                    </div>
                                                    <div class="btn-pay">
                                                        <button type="button" onclick="window.location.href='{{route('home')}}'">QUAY LẠI
                                                        </button>
                                                        <button type="submit">TIẾP TỤC</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="col-lg-5 tabs-right">
                                            <div class="view-order">
                                                <h4><i class="fas fa-shopping-cart"></i> XEM LẠI CHI TIẾT ĐƠN HÀNG</h4>
                                                <div class="content-cart">
                                                    <table class="table table-borderless">
                                                        <tbody>
                                                            <tr>
                                                                <td style="font-weight: 600;color: #B01803; width: 40%;">Purpose of visit: </td>
                                                                <td class="cf_purpose">{{isset($visa_new) ? ($visa_new->purpose()->first() != null ? $visa_new->purpose()->first()->name : "") : ""}}</td>
                                                            </tr>
                                                            <tr>
                                                                <td style="font-weight: 600;color: #B01803; width: 40%;">Visa type: </td>
                                                                <td class="cf_visa_type">
                                                                    {{isset($visa_new) ? ($visa_new->type()->first() != null ? $visa_new->type()->first()->name : "") : ""}}
                                                                </td>
                                                            </tr>
                                                            {{-- <tr>
                                                                <td style="font-weight: 600;color: #B01803; width: 40%;">Arrival airport:</td>
                                                                <td class="cf_arrival">{{isset($visa_new) ? $visa_new->cuakhau : ''}}</td>
                                                            </tr> --}}
                                                            <tr>
                                                                <td style="font-weight: 600;color: #B01803; width: 40%;">Processing time:</td>
                                                                <td class="cf_process">
                                                                    {{isset($visa_new) ? ($visa_new->processing()->first() != null ? $visa_new->processing()->first()->name : "") : ""}}
                                                                </td>
                                                            </tr>
                                                            {{-- <tr>
                                                                <td style="font-weight: 600;color: #B01803; width: 40%;">Number Person:</td>
                                                                <td>1</td>
                                                            </tr> --}}
                                                            <tr>
                                                                <td style="font-weight: 600;color: #B01803; width: 40%;">Visa fee:</td>
                                                                <td>50$ x 1 person= 50$</td>
                                                            </tr>
                                                            <tr>
                                                                <td style="font-weight: 600;color: #B01803; width: 40%;">Services fee: </td>
                                                                <td  class="service_fee"> </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="font-weight: 600;color: #B01803; width: 40%;">Code: </td>
                                                                <td class="cf_code"></td>
                                                            </tr>
                                                            <tr>
                                                                <td style="font-weight: 600;color: #B01803; width: 40%;">Express fee: </td>
                                                                <td class="express_fee">0$</td>
                                                            </tr>
                                                            
                                                            <tr style="border-top: 2px solid #B01803">
                                                                <td style="font-weight: 600;color: #B01803; width: 40%;">Total fee: </td>
                                                                <td class="total_fee">180 USD</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="step2" class="container-tabs"   {{$step == 3 ? 'style=display:block;' : 'style=display:none;'}} >
                                    <form class="form-horizontal" role="form"  
                                        action="{{route('update-visa')}}" method="POST">
                                        @csrf
                                    <div class="row">
                                        <div class="col-lg-7">
                                            <input type="hidden" name="visa_id" value="{{isset($visa_new) ? $visa_new->id : '' }}">
                                            <input type="hidden" name="service_support_1" id="service_support_1" value="{{isset($service_support)?$service_support:0}}">
                                            <input type="hidden" name="check" value="{{isset($check) ? $check : Session::get('check') }}">
                                            <h6>Chọn phương thức thanh toán</h6>
                                            <div class="row">
                                                <div class="form-group col-lg-6">
                                                    <select class="form-control" id="paymentForm" name="type_tt">
                                                        <option value="1" {{isset($visa_new) ? ($visa_new->type_tt == 1 ? 'selected' : '') : '' }}>Thanh toán tại văn phòng</option>
                                                        <option value="2"{{isset($visa_new) ? ($visa_new->type_tt == 2 ? 'selected' : '') : '' }}>PayPal</option>
                                                        <option value="3"{{isset($visa_new) ? ($visa_new->type_tt == 3 ? 'selected' : '') : '' }}>Chuyển khoản</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div id="transfer" class="content-pay-all content-pay">
                                                        <p style="margin: 0;">
                                                            <span style="color: #FFC200;font-weight: 500;">Trụ sở chính: </span><br/>
                                                            Địa chỉ: 33 phố Hàng Mắm – Phường Lý Thái Tổ - Quận Hoàn Kiếm – Tp.Hà Nội<br/>
                                                            Hotline:  <br/>

                                                            <span style="color: #FFC200;font-weight: 500;">Tp. Đà Nẵng: </span><br/>
                                                            Địa chỉ: 47 phố Nguyễn Chí Thanh - Quận Hải Châu – TP. Đà Nẵng<br/>
                                                            Hotline: <br/>
                                                            <span style="color: #FFC200;font-weight: 500;">Tp. HCM:</span> <br/>
                                                            Địa chỉ: Lầu 9 tòa nhà Vietcomreal – số 68 phố Nguyễn Huệ - Phường Bến Nghé – TP. HCM<br/>
                                                            Hotline:
                                                        </p>
                                                    </div>
                                                    <div id="card" class="content-pay-all content-pay-2" style="display: none">
                                                        <img src="{{asset('e-visa/images/pay-card.png')}}" alt="">
                                                    </div>
                                                    <div id="golbal" class="content-pay-all content-pay" style="display: none">
                                                        <p style="margin: 0;">
                                                            <span style="color: #FFC200;font-weight: 500;">Quốc tế: </span><br/>
                                                            Name Acount : DOAN HAI NAM<br/>
                                                            Bank : VIetcombank - Branch Hanoi<br/>
                                                            Switch Code : BFTVVNVX002<br/>
                                                            TK USD :  -0021370442274<br/>

                                                            <span style="color: #FFC200;font-weight: 500;">Trong nước: </span><br/>
                                                            TK VNĐ: 0011003141779<br/>
                                                            Nam Acount: ĐOÀN HẢI NAM<br/>
                                                            Ngân hàng: Vietcombank – chi nhánh Hà Nội<br/>
                                                            Swith Code: BFTVVNX002<br/>
                                                            Nội Dung: Thanh toán visa điện tử/ visa cửa khẩu – mã hồ sơ số: E/A 12345<br/>
                                                        </p>
                                                    </div>
                                                    <div class="btn-pay">
                                                        <button type= "submit">TIẾP TỤC</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-5 tabs-right">
                                            <div class="view-order">
                                                    <h4><i class="fas fa-shopping-cart"></i> XEM LẠI CHI TIẾT ĐƠN HÀNG</h4>
                                                    <div class="content-cart">
                                                        <table class="table table-borderless">
                                                            <tbody>
                                                                <tr>
                                                                    <td style="font-weight: 600;color: #B01803; width: 40%;">Purpose of visit: </td>
                                                                    <td class="cf_purpose">{{isset($visa_new) ? ($visa_new->purpose()->first() != null ? $visa_new->purpose()->first()->name : "") : ""}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="font-weight: 600;color: #B01803; width: 40%;">Visa type: </td>
                                                                    <td class="cf_visa_type">
                                                                        {{isset($visa_new) ? ($visa_new->type()->first() != null ? $visa_new->type()->first()->name : "") : ""}}
                                                                    </td>
                                                                </tr>
                                                                {{-- <tr>
                                                                    <td style="font-weight: 600;color: #B01803; width: 40%;">Arrival airport:</td>
                                                                    <td class="cf_arrival">{{isset($visa_new) ? $visa_new->cuakhau : ''}}</td>
                                                                </tr> --}}
                                                                <tr>
                                                                    <td style="font-weight: 600;color: #B01803; width: 40%;">Processing time:</td>
                                                                    <td class="cf_process">
                                                                        {{isset($visa_new) ? ($visa_new->processing()->first() != null ? $visa_new->processing()->first()->name : "") : ""}}
                                                                    </td>
                                                                </tr>
                                                                {{-- <tr>
                                                                    <td style="font-weight: 600;color: #B01803; width: 40%;">Number Person:</td>
                                                                    <td>1</td>
                                                                </tr> --}}
                                                                <tr>
                                                                    <td style="font-weight: 600;color: #B01803; width: 40%;">Visa fee:</td>
                                                                    <td>50$ x 1 person= 50$</td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="font-weight: 600;color: #B01803; width: 40%;">Services fee: </td>
                                                                    <td  class="service_fee"> </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="font-weight: 600;color: #B01803; width: 40%;">Code: </td>
                                                                    <td class="cf_code"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="font-weight: 600;color: #B01803; width: 40%;">Express fee: </td>
                                                                    <td class="express_fee">{{isset($service_support)?$service_support:"0"}}$</td>
                                                                </tr>
                                                                
                                                                <tr style="border-top: 2px solid #B01803">
                                                                    <td style="font-weight: 600;color: #B01803; width: 40%;">Total fee: </td>
                                                                    <td class="total_fee">180 USD</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                                    </form>
                                </div>
                                <div id="step3" class="container-tabs"  {{$step == 4 ? 'style=display:block;' : 'style=display:none;'}}>
                                    <div class="content-complate">
                                        <h6><i class="fas fa-check"></i> &nbsp;Chúc mừng Bạn đã hoàn thành apply online dịch vụ E-visa. Chúng tôi sẽ thông báo kết quả sớm nhất qua Email hoặc tại quản tra cứu kết quả</h6>
                                        <div class="view-order-complate">
                                            <h6><i class="fas fa-shopping-cart"></i> &nbsp;XEM LẠI CHI TIẾT ĐƠN HÀNG</h6>
                                            <div class="container">
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <div class="content-cart">
                                                            <table class="table table-borderless">
                                                                <tbody>
                                                                    <tr>
                                                                        <td style="font-weight: 600;color: #B01803; width: 40%;">Purpose of visit: </td>
                                                                        <td  class="cf_purpose">{{isset($visa_new) ? ($visa_new->purpose()->first() != null ? $visa_new->purpose()->first()->name : "") : ""}}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="font-weight: 600;color: #B01803; width: 40%;">Visa type: </td>
                                                                        <td class="cf_visa_type">{{isset($visa_new) ? ($visa_new->type()->first() != null ? $visa_new->type()->first()->name : "") : ""}}</td>
                                                                    </tr>
                                                                    {{-- <tr>
                                                                        <td style="font-weight: 600;color: #B01803; width: 40%;">Arrival airport:</td>
                                                                        <td class="cf_arrival">{{isset($visa_new) ? $visa_new->cuakhau : ''}}</td>
                                                                    </tr> --}}
                                                                    <tr>
                                                                        <td style="font-weight: 600;color: #B01803; width: 40%;">Processing time:</td>
                                                                        <td class="cf_process">{{isset($visa_new) ? ($visa_new->processing()->first() != null ? $visa_new->processing()->first()->name : "") : ""}}</td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <table class="table table-borderless">
                                                            <tbody>
                                                                <tr>
                                                                    <td style="font-weight: 600;color: #B01803; width: 40%;">Number Person:</td>
                                                                    <td>1</td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="font-weight: 600;color: #B01803; width: 40%;">Visa fee:</td>
                                                                    <td>50$ x 1 person= 50$</td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="font-weight: 600;color: #B01803; width: 40%;">Services fee: </td>
                                                                    <td class="service_fee"> </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="font-weight: 600;color: #B01803; width: 40%;">Code: </td>
                                                                    <td class="cf_code"></td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="font-weight: 600;color: #B01803; width: 40%;">Express fee: </td>
                                                                    <td class="express_fee">{{isset($service_support)?$service_support:"0"}}$</td>
                                                                </tr>
                                                                
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <table class="table table-borderless">
                                                            <tbody>
                                                                <tr style="border-top: 2px solid #B01803">
                                                                    <td style="font-weight: 600;color: #B01803; font-size: 18px;">Total fee: </td>
                                                                    <td style="font-weight: 600;color: #B01803; font-size: 18px;" class="total_fee">180 USD</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-12 text-center">
                                                        <button style="background: #B01803; border: none; color: #fff; margin: 20px 0; text-align: center; padding: 10px 15px; border-radius: 5px;"
                                                        type="button" onclick="window.location.href='{{route('home')}}'">VỀ TRANG CHỦ</button>
                                                        <button style="background: #4267b2; border: none; color: #fff; margin: 20px 0; text-align: center; padding: 10px 15px; border-radius: 5px;"
                                                        type="button" onclick="window.location.href='{{route('home')}}'">DỊCH VỤ THÊM</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@php
    $service_support=isset( $service_support)? $service_support:0;
    $arrType=json_encode($types);
    $arrNation=json_encode($nations);
    $arrPupose=json_encode($purposes);
    $arrProcess=json_encode($pross);
@endphp
<script>
    var check=<?php echo json_encode(isset($check)?$check:Session::get('check')); ?>;
    var service_support=<?php echo $service_support; ?>;
    var arrType=<?php echo $arrType; ?>;
    var arrNation=<?php echo $arrNation; ?>;
    var arrPupose=<?php echo $arrPupose; ?>;
    var arrProcess=<?php echo $arrProcess; ?>;
</script>
@include('font-end.partials.footer')
@include('font-end.partials.scripts')
</body>
</html>