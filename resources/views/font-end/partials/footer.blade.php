@php

    $about_us="";
    $link_icon=[];
    $quick_link=[];
    $address=[];
    $contact_phone="";
    $contact_fax="";
    foreach ($footer as $key => $value) {
        if($value->keyword==="about_us")
        {
            $about_us=$value;
        }
        else if($value->keyword==="link_icon"){
            array_push($link_icon,$value);
        }
        else if($value->keyword==="quick_link"){
            array_push($quick_link,$value);
        }
        else if($value->keyword==="address"){
            array_push($address,$value);
        }
        else if($value->keyword==="online_contact_phone"){
            $contact_phone=$value;
        }
        else if($value->keyword==="online_contact_fax"){
            $contact_fax=$value;
        }
    }
 
@endphp
<footer>
    <div class="footer-1">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-lg-8 col-xs-12">
                    <div class="call-to-action">
                        <div class="title-footer-1">
                            <h3>Call to action</h3>
                        </div>
                        <div class="row">
                            <div class="col-sm-7 box-form-footer-1">
                                <form action="">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Name">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Email">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Phone">
                                    </div>
                                </form>
                            </div>
                            <div class="col-sm-5">
                                <div class="warring-info">
                                    <p>
                                        Thông tin của bạn chỉ được chúng tôi sử dụng cho việc gửi thông tin từ E-visa tới bạn và không được tiết lộ cho bên thứ ba.
                                    </p>
                                    <button>brochure download</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 col-lg-4 col-xs-12">
                    <div class="travel-tour">
                        <div class="title-footer-1">
                            <h3>Travel tour</h3>
                        </div>
                        <div>
                            <p><span style="color: #173D66; font-weight: bold;">Public for everyone</span></p>
                            <p><span style="color: #808082;">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor</span></p>
                            <a href="https://www.namlongtour.com">https://www.namlongtour.com</a>
                            <button>Find more</button>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    <div class="footer-2">
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <div class="title">
                        <h3>About Us</h3>
                    </div>
                    <p>{{$about_us->content}}</p>
                    <ul class="socical">
                        @foreach ($link_icon as $item)
                            <li>
                                <a href="{{$item->content}}">
                                    <img src="{{asset($item->name)}}" alt="">
                                </a>
                            </li>
                        @endforeach
                      
                        {{-- <li>
                            <a href="#">
                                <img src="{{asset('e-visa/images/icon-fb.png')}}" alt="">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="{{asset('e-visa/images/icon-zalo.png')}}" alt="">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="{{asset('e-visa/images/icon-in.png')}}" alt="">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="{{asset('e-visa/images/icon-youtube.png')}}" alt="">
                            </a>
                        </li> --}}
                    </ul>
                </div>
                <div class="col-sm-4 quick-links">
                    <div class="title">
                        <h3>Quick links</h3>
                    </div>
                    <ul>
                        @foreach ($quick_link as $item)
                            <li>
                            <i class="fas fa-angle-right"></i><a href="{{$item->content}}">{{$item->name}}</a>
                            </li>
                        @endforeach
                        {{-- <li>
                            <i class="fas fa-angle-right"></i><a href="#">Extra Services </a>
                        </li>
                        <li>
                            <i class="fas fa-angle-right"></i><a href="#">Fast visa service </a>
                        </li>
                        <li>
                            <i class="fas fa-angle-right"></i><a href="#">Travel & Combo </a>
                        </li>
                        <li>
                            <i class="fas fa-angle-right"></i><a href="#">Visa on arrival</a>
                        </li>
                        <li>
                            <i class="fas fa-angle-right"></i><a href="#">Q&A</a>
                        </li> --}}
                    </ul>
                </div>
                <div class="col-sm-4 get-in-touch">
                    <div class="title">
                        <h3>Get in Touch</h3>
                    </div>
                    <p>
                        @foreach ($address as $item)
                    <span style="color: #e7b806"><i class="fas fa-home"></i> {{$item->name}}</span> {{$item->content}}<br/>
                        @endforeach
                        {{-- <span style="color: #e7b806"><i class="fas fa-home"></i> Trụ sở chính:</span> 33 phố Hàng Mắm – Phường Lý Thái Tổ Quận Hoàn Kiếm – Tp.Hà Nội<br/>
                        <span style="color: #e7b806"><i class="fas fa-home"></i>  Tp. Đà Nẵng:</span> 47 phố Nguyễn Chí Thanh Quận Hải Châu – TP. Đà Nẵng<br/>
                        <span style="color: #e7b806"><i class="fas fa-home"></i>  Tp. HCM:</span> Lầu 9 tòa nhà Vietcomreal – số 68 phố Nguyễn Huệ Phường Bến Nghé – TP. HCM --}}
                    </p>
                    <p>
                        <span style="color: #e7b806">
                            <i class="fas fa-phone-alt"></i> Phone: {{$contact_phone->content}} <br/>
                            <i class="fas fa-fax"></i> Fax: {{$contact_fax->content}}
                        </span>
                    </p>
                </div>
            </div>
        </div>
    </div>
</footer>