<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Processing extends Model
{
    //
    protected $fillable = [
        'name', 'value', 'status',"type_evisa","type_visa_arrival",
    ];
}
