<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('admin.gallery', function ($view) {
            $view->with(['flag'=>'files', 'title'=>'Files']);
        });
        /** Nation **/
        View::composer('admin.nation.list', function ($view) {
            $view->with(['flag'=>'nation', 'title'=>'Danh sách : Nationality',
                        'route_create' => route('nation.create')
                    ]);
        });
        View::composer('admin.nation.create', function ($view) {
            $view->with(['flag'=>'nation', 'title'=>'Tạo mới : Nationality',
                        'route_list' => route('nation.index'),
                        'method'=>'POST', 'btn_submit' => 'Tạo mới']);
        });
        View::composer('admin.nation.edit', function ($view) {
            $view->with(['flag'=>'nation', 'title'=>'Sửa đổi : Nationality',
                        'route_list' => route('nation.index'),
                        'method'=>'PATCH', 'btn_submit' => 'Cập nhật']);
        });
        /** -------- **/
        
        /** Purpose **/
        View::composer('admin.purpose.list', function ($view) {
            $view->with(['flag'=>'purpose', 'title'=>'Danh sách : Purpose',
                        'route_create' => route('purpose.create')
                    ]);
        });
        View::composer('admin.purpose.create', function ($view) {
            $view->with(['flag'=>'purpose', 'title'=>'Tạo mới : Purpose',
                        'route_list' => route('purpose.index'),
                        'method'=>'POST', 'btn_submit' => 'Tạo mới']);
        });
        View::composer('admin.purpose.edit', function ($view) {
            $view->with(['flag'=>'purpose', 'title'=>'Sửa đổi : Purpose',
                        'route_list' => route('purpose.index'),
                        'method'=>'PATCH', 'btn_submit' => 'Cập nhật']);
        });
        /** ---------- **/

        /** Type **/
        View::composer('admin.type.list', function ($view) {
            $view->with(['flag'=>'type', 'title'=>'Danh sách : Type',
                        'route_create' => route('type.create')
                    ]);
        });
        View::composer('admin.type.create', function ($view) {
            $view->with(['flag'=>'type', 'title'=>'Tạo mới : Type',
                        'route_list' => route('type.index'),
                        'method'=>'POST', 'btn_submit' => 'Tạo mới']);
        });
        View::composer('admin.type.edit', function ($view) {
            $view->with(['flag'=>'type', 'title'=>'Sửa đổi : Type',
                        'route_list' => route('type.index'),
                        'method'=>'PATCH', 'btn_submit' => 'Cập nhật']);
        });

         /** Processing **/
        View::composer('admin.processing.list', function ($view) {
            $view->with(['flag'=>'processing', 'title'=>'Danh sách : Processing',
                        'route_create' => route('processing.create')
                    ]);
        });
        View::composer('admin.processing.create', function ($view) {
            $view->with(['flag'=>'processing', 'title'=>'Tạo mới : Processing',
                        'route_list' => route('processing.index'),
                        'method'=>'POST', 'btn_submit' => 'Tạo mới']);
        });
        View::composer('admin.processing.edit', function ($view) {
            $view->with(['flag'=>'processing', 'title'=>'Sửa đổi : Processing',
                        'route_list' => route('processing.index'),
                        'method'=>'PATCH', 'btn_submit' => 'Cập nhật']);
        });

        /** Visa **/
        View::composer('admin.customer.list', function ($view) {
            $view->with(['flag'=>'customer', 'title'=>'Danh sách : Đơn hàng',
                        'route_create' => '#'
                    ]);
        });
    }
}
