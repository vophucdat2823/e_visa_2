<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    //
    protected $fillable = [
        'name', 'pass_number', 'cuakhau', 'date_in', 'phone', 'email', 'address', 'note',
        'nation_id', 'processing_id', 'purpose_id', 'type_id', 'type_tt', 'status','service_support','type'
    ];
    public function nation()
    {
    	return $this->belongsto('App\Nation');
    }
    public function processing()
    {
    	return $this->belongsto('App\Processing');
    }
    public function purpose()
    {
    	return $this->belongsto('App\Purpose');
    }
    public function type()
    {
    	return $this->belongsto('App\Type');
    }
}
