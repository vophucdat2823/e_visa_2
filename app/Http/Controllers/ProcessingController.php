<?php

namespace App\Http\Controllers;

use App\Processing;
use Illuminate\Http\Request;
use App\Http\Requests\ProcessingRequest as ProcessingRequest;
use Session;

class ProcessingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $objs = Processing::all();
        return view('admin.processing.list',['objs'=>$objs]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.processing.create',['route_action'=>route('processing.store')]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProcessingRequest $request)
    {
        $input=$request->all();
        if($input["typevisa"]==="1")
        {
            $input["type_evisa"]=1;
        }
        else if($input["typevisa"]==="2")
        {
            $input["type_visa_arrival"]=1;
        }
        else
        {
            $input["type_evisa"]=1;
            $input["type_visa_arrival"]=1;
        }
        Processing::create($input);
        Session::flash('success-processing', 'Tạo mới processing : "'.$input['name'].'" thành công.');
        return redirect()->route('processing.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Nation  $nation
     * @return \Illuminate\Http\Response
     */
    public function show(Nation $nation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Nation  $nation
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $obj = Processing::find($id);
        if($obj == null){
            Session::flash('error-processing', 'Không tìm thấy dữ liệu.');  
            return redirect()->route('processing.index');  
        }
        return view('admin.processing.edit',['obj'=>$obj, 
                    'route_action'=>route('processing.update',['id'=>$id])]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Nation  $nation
     * @return \Illuminate\Http\Response
     */
    public function update(ProcessingRequest $request, $id)
    {
        //
        $obj = Processing::find($id);
        if($obj == null){
            Session::flash('error-processing', 'Không tìm thấy dữ liệu.');  
            return redirect()->route('processing.index');  
        }
        $input=$request->all();
        if($input["typevisa"]==="1")
        {
            $input["type_evisa"]=1;
            $input["type_visa_arrival"]=0;
        }
        else if($input["typevisa"]==="2")
        {
            $input["type_visa_arrival"]=1;
            $input["type_evisa"]=0;
        }
        else
        {
            $input["type_evisa"]=1;
            $input["type_visa_arrival"]=1;
        }
        $obj->update($input);
        Session::flash('success-processing', 'Cập nhật dữ liệu thành công.'); 
        return redirect()->route('processing.edit',['id'=>$id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Nation  $nation
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $obj = Processing::find($id);
        if($obj == null){
            Session::flash('error-processing', 'Không tìm thấy dữ liệu.');  
            return redirect()->route('processing.index');  
        }
        $obj->delete();
        Session::flash('success-processing', 'Xóa dữ liệu thành công.');  
        return redirect()->route('processing.index');  
    }
}
