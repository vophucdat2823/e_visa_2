<?php

namespace App\Http\Controllers;

use App\Type;
use Illuminate\Http\Request;
use App\Http\Requests\TypeRequest as TypeRequest;
use Session;

class TypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $objs = Type::all();
        return view('admin.type.list',['objs'=>$objs]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.type.create',['route_action'=>route('type.store')]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TypeRequest $request)
    {
        $input=$request->all();
        if($input["typevisa"]==="1")
        {
            $input["type_evisa"]=1;
        }
        else if($input["typevisa"]==="2")
        {
            $input["type_visa_arrival"]=1;
        }
        else
        {
            $input["type_evisa"]=1;
            $input["type_visa_arrival"]=1;
        }
        Type::create($input);
        Session::flash('success-type', 'Tạo mới type : "'.$input['name'].'" thành công.');
        return redirect()->route('type.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Nation  $nation
     * @return \Illuminate\Http\Response
     */
    public function show(Nation $nation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Nation  $nation
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $obj = Type::find($id);
        if($obj == null){
            Session::flash('error-type', 'Không tìm thấy dữ liệu.');  
            return redirect()->route('type.index');  
        }
        return view('admin.type.edit',['obj'=>$obj, 
                    'route_action'=>route('type.update',['id'=>$id])]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Nation  $nation
     * @return \Illuminate\Http\Response
     */
    public function update(TypeRequest $request, $id)
    {
        //
        $obj = Type::find($id);
        if($obj == null){
            Session::flash('error-type', 'Không tìm thấy dữ liệu.');  
            return redirect()->route('type.index');  
        }
        $input=$request->all();
        if($input["typevisa"]==="1")
        {
            $input["type_evisa"]=1;
            $input["type_visa_arrival"]=0;
        }
        else if($input["typevisa"]==="2")
        {
            $input["type_visa_arrival"]=1;
            $input["type_evisa"]=0;
        }
        else
        {
            $input["type_evisa"]=1;
            $input["type_visa_arrival"]=1;
        }
        $obj->update($input);
        Session::flash('success-type', 'Cập nhật dữ liệu thành công.'); 
        return redirect()->route('type.edit',['id'=>$id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Nation  $nation
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $obj = Type::find($id);
        if($obj == null){
            Session::flash('error-type', 'Không tìm thấy dữ liệu.');  
            return redirect()->route('type.index');  
        }
        $obj->delete();
        Session::flash('success-type', 'Xóa dữ liệu thành công.');  
        return redirect()->route('type.index');  
    }
}
