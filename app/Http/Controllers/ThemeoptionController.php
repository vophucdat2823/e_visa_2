<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;
class ThemeoptionController extends Controller
{
    public function indexHeader()
    {
        $flag="header";
        $title="Theme Option | Danh Sách";
        $theme_header=DB::select("SELECT * FROM `theme` where group_name='HEADER'");
        $quick_link=DB::select("SELECT * FROM `theme` where keyword='quick_link'");
        $link_icon=DB::select("SELECT * FROM `theme` where keyword='link_icon'");
        $theme_footer=DB::select("SELECT * FROM `theme` where group_name='FOOTER' && keyword != 'quick_link' && keyword != 'link_icon' ");
      
        return view("admin.theme.index",compact(array('flag','title','theme_header','link_icon','quick_link','theme_footer')));
    }
    public function indexEdit(Request $request)
    {
        $id=isset($request->id) && $request->id !==""?$request->id: false;
        if(!$id){
            return redirect()->back();
        }
        $theme=DB::select("SELECT * FROM `theme` where id=?",[$id]);
        $theme=isset($theme[0])?$theme[0]:false;
        if(!$theme){
            return redirect()->back();
        }
        $flag='header';
        $title="Theme Option | Chỉnh Sửa";
        return view("admin.theme.edit",compact(array('title','theme','flag')));
    }
    public function postEdit(Request $request)
    {
        $id=isset($request->id) && $request->id !==""?$request->id: false;
        if(!$id){
            Session::flash('error-theme', 'Máy chủ bị lỗi! Vui lòng thử lại sau.');  
            return redirect()->back();
        }
        $keyword=isset($request->keyword) && $request->keyword !==""?$request->keyword: false;
        if(!$keyword){
            Session::flash('error-theme', 'Máy chủ bị lỗi! Vui lòng thử lại sau.');  
            return redirect()->back();
        }
        if($keyword==='logo' || $keyword==='banner')
        {
            $file_image = isset($request->image) && $request->image !=null ? $request->image : '';
            if($file_image==='')
            {
                Session::flash('error-theme', 'Hình ảnh không được rỗng');  
                return redirect()->back();
            }
            else{
                $imageName = time().'.'.$request->image->getClientOriginalExtension();
                $content="/public/upload/".$imageName;
                DB::update("UPDATE `theme` SET content=? WHERE id=?",[$content,$id]);
                $request->image->move(public_path()."/upload/", $imageName);
                Session::flash('success-theme', 'Thay đổi thành công');  
                return redirect()->route('indexHeader');
            }
        }
        else{
            $content=isset($request->content) && $request->content !==""?$request->content: false;
            if(!$content){
                Session::flash('error-theme', 'Nội dung không được rỗng.');  
                return redirect()->back();
            }
            $update=DB::update("UPDATE `theme` SET content=? WHERE id=?",[$content,$id]);
            
            Session::flash('success-theme', 'Thay đổi thành công');  
            return redirect()->route('indexHeader');

        }
    }
}
