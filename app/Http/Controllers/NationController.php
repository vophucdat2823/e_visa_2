<?php

namespace App\Http\Controllers;

use App\Nation;
use Illuminate\Http\Request;
use App\Http\Requests\NationRequest as NationRequest;
use Session;

class NationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $objs = Nation::all();
        return view('admin.nation.list',['objs'=>$objs]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.nation.create',['route_action'=>route('nation.store')]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(NationRequest $request)
    {
        $input=$request->all();
        if($input["typevisa"]==="1")
        {
            $input["type_evisa"]=1;
        }
        else if($input["typevisa"]==="2")
        {
            $input["type_visa_arrival"]=1;
        }
        else
        {
            $input["type_evisa"]=1;
            $input["type_visa_arrival"]=1;
        }
      
        Nation::create($input);
        Session::flash('success-nation', 'Tạo mới nation : "'.$input['name'].'" thành công.');
        return redirect()->route('nation.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Nation  $nation
     * @return \Illuminate\Http\Response
     */
    public function show(Nation $nation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Nation  $nation
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $obj = Nation::find($id);
        if($obj == null){
            Session::flash('error-nation', 'Không tìm thấy dữ liệu.');  
            return redirect()->route('nation.index');  
        }
        return view('admin.nation.edit',['obj'=>$obj, 
                    'route_action'=>route('nation.update',['id'=>$id])]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Nation  $nation
     * @return \Illuminate\Http\Response
     */
    public function update(NationRequest $request, $id)
    {
        //
        $obj = Nation::find($id);
        if($obj == null){
            Session::flash('error-nation', 'Không tìm thấy dữ liệu.');  
            return redirect()->route('nation.index');  
        }
        $input=$request->all();
        if($input["typevisa"]==="1")
        {
            $input["type_evisa"]=1;
            $input["type_visa_arrival"]=0;
        }
        else if($input["typevisa"]==="2")
        {
            $input["type_visa_arrival"]=1;
            $input["type_evisa"]=0;
        }
        else
        {
            $input["type_evisa"]=1;
            $input["type_visa_arrival"]=1;
        }
        $obj->update($input);
        Session::flash('success-nation', 'Cập nhật dữ liệu thành công.'); 
        return redirect()->route('nation.edit',['id'=>$id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Nation  $nation
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $obj = Nation::find($id);
        if($obj == null){
            Session::flash('error-nation', 'Không tìm thấy dữ liệu.');  
            return redirect()->route('nation.index');  
        }
        $obj->delete();
        Session::flash('success-nation', 'Xóa dữ liệu thành công.');  
        return redirect()->route('nation.index');  
    }
}
