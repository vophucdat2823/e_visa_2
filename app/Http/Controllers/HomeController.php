<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;
use App\Http\Requests\CustomerRequest as CustomerRequest;
use Session;
use App\Nation;
use App\Purpose;
use App\Type;
use App\Processing;
use DB;
use View;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $header=DB::select("SELECT * from `theme` where group_name='HEADER' ");
        $footer=DB::select("SELECT * from `theme` where group_name='FOOTER' ");
        View::share('header', $header);
        View::share('footer', $footer);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $nations = Nation::where('status',1)->get();
        $purposes = Purpose::where('status',1)->get();
        $types = Type::where('status',1)->get();
        $process = Processing::where('status',1)->get();

        return view('font-end.index',['nations'=>$nations, 'purposes'=>$purposes, 
                                        'types'=>$types, 'process'=>$process]);
    }
    public function procedure()
    {
        return view('font-end.huongdan');
    }

    
    public function createVisa(CustomerRequest $request)
    {
        $id = $request->visa_id;
        $arr = $request->all();
        $arr['type']=$arr['check'];
        if(isset($arr["phone"]) && $arr["my-country"]!==null)
        {
            $arr["phone"]=$arr["my-country"].$arr["phone"];
        }
        $service_support=0;
        if(isset($arr["service_support"]))
        {
            $service_support=25;
        }
        $arr['status'] = 0;
        if($id != null && $id != ''){
            $visa_new = Customer::find($id);
            if($visa_new != null){
                $visa_new->update( $arr);
                if(isset($request->fileprofile) && $request->fileprofile!=null)
                {
                    $imageName = time().'.'.$request->fileprofile->getClientOriginalExtension();
                    $content="/public/upload/".$imageName;
                    $request->fileprofile->move(public_path()."/upload/", $imageName);
                    DB::update("UPDATE `customers` SET fileprofile=? WHERE id=?",[$content,$id]);
                }
                if(isset($request->filepassport) && $request->filepassport!=null)
                {
                    $imageName = time().'.'.$request->filepassport->getClientOriginalExtension();
                    $content="/public/upload/".$imageName;
                    $request->filepassport->move(public_path()."/upload/", $imageName);
                    DB::update("UPDATE `customers` SET filepassport=? WHERE id=?",[$content,$id]);
                }
            }
        }else{
            $visa_new = Customer::create($arr);  
            $new_id=$visa_new->id;
            if(isset($request->fileprofile) && $request->fileprofile!=null)
            {
                $imageName = time().'.'.$request->fileprofile->getClientOriginalExtension();
                $content="/public/upload/".$imageName;
                $request->fileprofile->move(public_path()."/upload/", $imageName);
                DB::update("UPDATE `customers` SET fileprofile=? WHERE id=?",[$content,$new_id]);
            }
            if(isset($request->filepassport) && $request->filepassport!=null)
            {
                $imageName = time().'.'.$request->filepassport->getClientOriginalExtension();
                $content="/public/upload/".$imageName;
                $request->filepassport->move(public_path()."/upload/", $imageName);
                DB::update("UPDATE `customers` SET filepassport=? WHERE id=?",[$content,$new_id]);
            }
        }  
        $nations = Nation::where('status',1)->get();
        $purposes = Purpose::where('status',1)->get();
        $types = Type::where('status',1)->get();
        $pross = Processing::where('status',1)->get();
        $step  = 3;
        if($arr['check']==="visa_arrival"){
            return view('font-end.step2-arrival',['visa_new' =>$visa_new, 'step'=>$step,'nations'=>$nations, 'purposes'=>$purposes, 'types'=>$types, 'pross'=>$pross,'service_support'=>$service_support,"check"=>$arr['check']]);
        }
        return view('font-end.step2',['visa_new' =>$visa_new, 'step'=>$step,'nations'=>$nations, 'purposes'=>$purposes, 'types'=>$types, 'pross'=>$pross,'service_support'=>$service_support,"check"=>$arr['check']]);
    }
    public function updateVisa(Request $request)
    {
        $id = $request->visa_id;
        $check=$request->check;
        $type_tt = $request->type_tt;
        $nations = Nation::where('status',1)->get();
        $purposes = Purpose::where('status',1)->get();
        $service_support=$request->service_support_1;
        $types = Type::where('status',1)->get();
        $pross = Processing::where('status',1)->get();
        if($id != null && $id != ''){
            $visa = Customer::find($id);
            if($visa != null){
                $visa->type_tt = $type_tt;
                $visa->update();
                $step = 4;
                if($check==="visa_arrival"){
                    return view('font-end.step2-arrival',['visa_new' =>$visa, 'step'=>$step,'nations'=>$nations, 'purposes'=>$purposes, 'types'=>$types, 'pross'=>$pross,"service_support"=>$service_support,"check"=>$check]);
                }
                return view('font-end.step2',['visa_new' =>$visa, 'step'=>$step,'nations'=>$nations, 'purposes'=>$purposes, 'types'=>$types, 'pross'=>$pross,"service_support"=>$service_support,"check"=>$check]);
            }
        }
        $step  = 2;
        if($check==="visa_arrival"){
            return view('font-end.step2-arrival',['step'=>$step,'nations'=>$nations, 'purposes'=>$purposes, 'types'=>$types, 'pross'=>$pross]);
        }
        return view('font-end.step2',['step'=>$step,'nations'=>$nations, 'purposes'=>$purposes, 'types'=>$types, 'pross'=>$pross]);
    }
    public function step1(Request $request)
    {    
        session(['nation' => $request->nation]);
        session(['purpose' => $request->purpose]);
        session(['type' => $request->type]);
        session(['processing' => $request->process]);
        session(['check' => $request->check]);
        $check=$request->check;
        return view('font-end.step1',compact('check'));
    }

    public function step2(Request $request)
    {
        $nations = Nation::where([['status',1],["type_evisa",1]])->get();
        $purposes = Purpose::where([['status',1],["type_evisa",1]])->get();
        $types = Type::where([['status',1],["type_evisa",1]])->get();
        $pross = Processing::where([['status',1],["type_evisa",1]])->get();
        $step  = 2;
        $check="evisa";
        return view('font-end.step2',['step'=>$step,'nations'=>$nations, 'purposes'=>$purposes, 'types'=>$types, 'pross'=>$pross,"check"=>$check]);
    }
    public function step2_arrival(Request $request)
    {
        $nations = Nation::where([['status',1],["type_visa_arrival",1]])->get();
        $purposes = Purpose::where([['status',1],["type_visa_arrival",1]])->get();
        $types = Type::where([['status',1],["type_visa_arrival",1]])->get();
        $pross = Processing::where([['status',1],["type_visa_arrival",1]])->get();
        $step  = 2;
        $check="visa_arrival";
        return view('font-end.step2-arrival',['step'=>$step,'nations'=>$nations, 'purposes'=>$purposes, 'types'=>$types, 'pross'=>$pross,"check"=>$check]);
    }
}
