<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Nation;
use App\Processing;
use App\Purpose;
use App\Type;
use Illuminate\Http\Request;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use Session;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $visas = Customer::where('status','!=',2)->orderBy("id","desc")->get();
        return view('admin.customer.list',['objs'=>$visas]);
    }

    public function updateStatus(Request $request, $id)
    {
        $obj = Customer::where('status', 0)->where('id', $id)->first();
        if($obj == null){
            Session::flash('error-customer', 'Không tìm thấy dữ liệu.');  
            return redirect()->route('customer.index');  
        }
        $type = $request->type_status;
        if($type == 1){
            $content = "Đơn hàng của bạn đã được chấp thuận.";
        }else{
            $content = "Đơn hàng của bạn đã bị từ chối.";
        }
        $obj->status = $type;
        $obj->update();
        $mail = new PHPMailer(true);
        try 
            {
                //Server settings
                $mail->CharSet = 'UTF-8';                                       // Enable verbose debug output
                $mail->isSMTP();                                            // Set mailer to use SMTP
                $mail->Host       = 'smtp.gmail.com';  // Specify main and backup SMTP servers
                $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
                $mail->Username   = 'nm.dung.1991@gmail.com';                     // SMTP username
                $mail->Password   = 'hmhnfmkwrggxhxhi';                               // SMTP password
                $mail->SMTPSecure = 'tls';                                  // Enable TLS encryption, `ssl` also accepted
                $mail->Port       = 587;                                    // TCP port to connect to
                $mail->smtpConnect(
                array(
                    "ssl" => array(
                        "verify_peer" => false,
                        "verify_peer_name" => false,
                        "allow_self_signed" => true
                    )
                )
                );
                //Recipients
                $mail->setFrom('nm.dung.1991@gmail.com', 'E-VISA VIỆT NAM');
                $mail->addAddress($obj->email);              // Name is optional
                $mail->isHTML(true);                                  // Set email format to HTML
                $mail->Subject = 'Thông báo trạng thái đơn hàng';
                $mail->Body    = $content;
                $mail->send();
            } catch (Exception $e) {
            }
        Session::flash('success-customer', 'Update dữ liệu thành công.');  
        return redirect()->route('customer.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function show(Customer $customer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function edit(Customer $customer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Customer $customer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Customer  $customer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Customer $customer)
    {
        //
    }
    public function detailOrder(Request $request,$id)
    {
        $order=Customer::find($id)->toarray();
        if($order)
        {
            $flag="customer";
            $title="Đơn hàng | Chi tiết";
            $nation=Nation::find($order["nation_id"]);
            $processing=Processing::find($order["processing_id"]);
            $purpose=Purpose::find($order["purpose_id"]);
            $type=Type::find($order["type_id"]);

            $order['nation']=$nation;
            $order['processing']=$processing;
            $order['purpose']=$purpose;
            $order['type']=$type;
            return view('admin.customer.detail',['order'=>$order,"title"=>$title,"flag"=>$flag]);
        }
        else{

        }    
    }
}
