<?php

namespace App\Http\Controllers;

use App\Purpose;
use Illuminate\Http\Request;
use App\Http\Requests\PurposeRequest as PurposeRequest;
use Session;

class PurposeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $objs = Purpose::all();
        return view('admin.purpose.list',['objs'=>$objs]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.purpose.create',['route_action'=>route('purpose.store')]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PurposeRequest $request)
    {
        $input=$request->all();
        if($input["typevisa"]==="1")
        {
            $input["type_evisa"]=1;
        }
        else if($input["typevisa"]==="2")
        {
            $input["type_visa_arrival"]=1;
        }
        else
        {
            $input["type_evisa"]=1;
            $input["type_visa_arrival"]=1;
        }
      
        Purpose::create($input);
        Session::flash('success-purpose', 'Tạo mới purpose : "'.$input['name'].'" thành công.');
        return redirect()->route('purpose.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Purpose  $purpose
     * @return \Illuminate\Http\Response
     */
    public function show(Purpose $purpose)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Purpose  $purpose
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $obj = Purpose::find($id);
        if($obj == null){
            Session::flash('error-purpose', 'Không tìm thấy dữ liệu.');  
            return redirect()->route('purpose.index');  
        }
        return view('admin.purpose.edit',['obj'=>$obj, 
                    'route_action'=>route('purpose.update',['id'=>$id])]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Purpose  $purpose
     * @return \Illuminate\Http\Response
     */
    public function update(PurposeRequest $request, $id)
    {
        //
        $obj = Purpose::find($id);
        if($obj == null){
            Session::flash('error-purpose', 'Không tìm thấy dữ liệu.');  
            return redirect()->route('purpose.index');  
        }
        $input=$request->all();
        if($input["typevisa"]==="1")
        {
            $input["type_evisa"]=1;
            $input["type_visa_arrival"]=0;
        }
        else if($input["typevisa"]==="2")
        {
            $input["type_visa_arrival"]=1;
            $input["type_evisa"]=0;
        }
        else
        {
            $input["type_evisa"]=1;
            $input["type_visa_arrival"]=1;
        }
        $obj->update($input);
        Session::flash('success-purpose', 'Cập nhật dữ liệu thành công.'); 
        return redirect()->route('purpose.edit',['id'=>$id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Purpose  $purpose
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $obj = Purpose::find($id);
        if($obj == null){
            Session::flash('error-purpose', 'Không tìm thấy dữ liệu.');  
            return redirect()->route('purpose.index');  
        }
        $obj->delete();
        Session::flash('success-purpose', 'Xóa dữ liệu thành công.');  
        return redirect()->route('purpose.index'); 
    }
}
