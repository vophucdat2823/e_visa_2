$(document).ready(function(){
	$('.delete-button').click(function(e) 
	{ 
		action = $(this).data('action');
		$('#deleteModal').find('#modal-form-delete').attr('action', action);
		$('#deleteModal').modal('show');
	})
	$('.status-button').click(function(e) 
	{ 
		action = $(this).data('action');
		type = $(this).data('type');
		if(type == 1){
			msg = "Bạn muốn tán thành đơn hàng đã chọn.";
		}else{
			msg = "Bạn muốn từ chối đơn hàng đã chọn.";
		}
		$('#type_status').val(type);
		$('#status-modal-title').html(msg);
		$('#statusModal').find('#modal-form-status').attr('action', action);
		$('#statusModal').modal('show');
	})
});