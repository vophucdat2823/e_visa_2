jQuery(document).ready(function($) {

 jQuery('.menu-responsive .menu-item-has-children > a').after('<span class="sub-open"></span>');
 jQuery('.menu-responsive .sub-open').click( function () {
  jQuery(this).closest('li').children('.sub-menu').toggle(600);
  jQuery(this).toggleClass('sub-opend');
});
 jQuery('#header-search-button-mb, .hrm-search-close').click( function () {
   jQuery('body').toggleClass('search-opened');
 });
 /* menu responsive*/
 jQuery('.menu-open').click( function () {
  jQuery('.menu-responsive, .menu-responsive-overlay').toggleClass('show-mn');
});
 jQuery('.menu-close, .menu-responsive-overlay').click( function () {
  jQuery('.menu-responsive, .menu-responsive-overlay').toggleClass('show-mn');
});
 jQuery('.class-slide').lightSlider({
  item:2,
  slideMargin:7,
  auto:false,
  pause: 3000,
  speed: 1000,
  pager: false,
  loop:true,
  prevHtml:'<i class="fa fa-angle-left"></i>',
  nextHtml:'<i class="fa fa-angle-right"></i>',
  responsive : [
  {
    breakpoint:1920,
    settings: {
      item:2,
      slideMove:1,
      slideMargin:13,
    }
  },
  {
    breakpoint:480,
    settings: {
      item:1,
      slideMove:1
    }
  }
  ]
});
 jQuery('.class-slide-hot').lightSlider({
  item:4,
  slideMargin:7,
  auto:false,
  pause: 3000,
  speed: 1000,
  pager: false,
  loop:true,
  prevHtml:'<i class="fa fa-angle-left"></i>',
  nextHtml:'<i class="fa fa-angle-right"></i>',
  responsive : [
  {
    breakpoint:1920,
    settings: {
      item:4,
      slideMove:1,
      slideMargin:13,
    }
  },
  {
    breakpoint:768,
    settings: {
      item:2,
      slideMove:1,
      slideMargin:10,
    }
  },
  {
    breakpoint:480,
    settings: {
      item:1,
      slideMove:1
    }
  }
  ]
});
 jQuery('.cmt-slide').lightSlider({
  item:1,
  slideMargin:0,
  auto:false,
  pause: 3000,
  speed: 1000,
  pager: true,
  loop:true,
  prevHtml:'<i class="fa fa-angle-left"></i>',
  nextHtml:'<i class="fa fa-angle-right"></i>',
});
 jQuery('.related-post-slide').lightSlider({
  item:1,
  slideMargin:0,
  auto:false,
  pause: 3000,
  speed: 1000,
  pager: false,
  loop:true,
  prevHtml:'<i class="fa fa-angle-left"></i>',
  nextHtml:'<i class="fa fa-angle-right"></i>',
});
  jQuery('.hot-post-slide').lightSlider({
  item:3,
  slideMargin:0,
  auto:false,
  pause: 3000,
  speed: 1000,
  pager: false,
  loop:true,
  prevHtml:'<i class="fa fa-angle-left"></i>',
  nextHtml:'<i class="fa fa-angle-right"></i>',
});
 jQuery('.pageabout-slide').lightSlider({
  item:1,
  slideMargin:0,
  mode: 'fade',
  auto:true,
  pause: 3000,
  speed: 1300,
  pager: true,
  loop:true,
  controls: false,
});
 jQuery().fancybox({
  selector : '[data-fancybox="gallery"], .gallery-item a',
  loop     : true,
});
});