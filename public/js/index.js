
var fee=0;
if(check==="visa_arrival")
{
	fee=50
}
else{
	fee=25
}
function renderType(arrType,type='arrival'){
	$("#type").empty();
	for (let index = 0; index < arrType.length; index++) {
		const element = arrType[index];
		if(type==='evisa'){
			if(element.type_evisa==1)
			{
				let html=`<option data-value="${element.value}" value="${element.id}">${element.name}</option>`
				$("#type").append(html);
			}
		}
		else{
			if(element.type_visa_arrival==1)
			{
				let html=`<option data-value="${element.value}" value="${element.id}">${element.name}</option>`
				$("#type").append(html);
			}
		}
		
	}
}

function renderPupose(arrPupose,type='arrival'){
	$("#purpose").empty();
	for (let index = 0; index < arrPupose.length; index++) {
		const element = arrPupose[index];
		if(type==='evisa'){
			if(element.type_evisa==1)
			{
				let html=`<option data-value="${element.value}" value="${element.id}">${element.name}</option>`
				$("#purpose").append(html);
			}
		}
		else{
			if(element.type_visa_arrival==1)
			{
				let html=`<option data-value="${element.value}" value="${element.id}">${element.name}</option>`
				$("#purpose").append(html);
			}
		}
		
	}
}
function renderProcess(arrProcess,type='arrival'){
	$("#process").empty();
	for (let index = 0; index < arrProcess.length; index++) {
		const element = arrProcess[index];
		if(type==='evisa'){
			if(element.type_evisa==1)
			{
				let html=`<option data-value="${element.value}" value="${element.id}">${element.name}</option>`
				$("#process").append(html);
			}
		}
		else{
			if(element.type_visa_arrival==1)
			{
				let html=`<option data-value="${element.value}" value="${element.id}">${element.name}</option>`
				$("#process").append(html);
			}
		}
		
	}
}
// ======================= Trang chủ===========================
renderType(arrType,'evisa');
renderPupose(arrPupose,'evisa');
renderProcess(arrProcess,'evisa');

nation = $('#nation option:selected').data("value");
purpose = $('#purpose option:selected').data("value");
type = $('#type option:selected').data("value");
processing = $('#process option:selected').data("value");

sum = 0;
if(nation != null && nation != ""){
	sum = sum + parseFloat(nation);
	$('#pre_nation').html(' $'+nation);
}
if(purpose !== null && purpose !== ""){
	sum = sum + parseFloat(purpose);
	$('#pre_purpose').html(' $'+purpose);
}
if(type != null && type != ""){
	sum = sum + parseFloat(type);
	$('#pre_type').html(' $'+type);
}
if(processing != null && processing != ""){
	sum = sum + parseFloat(processing);
	$('#pre_pro').html(' $'+processing);
}

$('#pre_sum').html(' $'+sum);

// change select

$('#nation, #purpose, #type, #process').change( function() {
	nation = $('#nation option:selected').data("value");
	if(nation==="orther")
	{
		nation=-1;
		if($("#check").val()!=="visa_arrival")
		{
			renderType(arrType);
			renderPupose(arrPupose);
			renderProcess(arrProcess);
		}
		$("#check").val("visa_arrival");
	}
	else{
		if($("#check").val()!=="evisa")
		{
			renderType(arrType,'evisa');
			renderPupose(arrPupose,'evisa');
			renderProcess(arrProcess,'evisa');
		}
		$("#check").val("evisa");
	}
	purpose = $('#purpose option:selected').data("value");
	type = $('#type option:selected').data("value");
	processing = $('#process option:selected').data("value");
	sum = 0;

	if(nation != null && nation != ""){
		if(nation===-1){
			nation=0;
		}
		sum = sum + parseFloat(nation);
		$('#pre_nation').html(' $'+nation);
	}
	if(purpose !== null && purpose !== ""){
		
		sum = sum + parseFloat(purpose);
		$('#pre_purpose').html(' $'+purpose);
	}
	if(type !== null && type !== ""){
		sum = sum + parseFloat(type);
		$('#pre_type').html(' $'+type);
	}
	if(processing !== null && processing !== ""){
		sum = sum + parseFloat(processing);
		$('#pre_pro').html(' $'+processing);
	}
	$('#pre_sum').html(' $'+sum);
});
// ======================= Trang chủ===========================

var cf_code=randomNumber();
var base_url = $('#base_url').val();
$('#btn_next').click(function(){
	window.location = base_url + 'step-2.html';
});
str_nation = $('#nation_id option:selected').text();
str_purpose = $('#purpose_id option:selected').text();
str_type = $('#type_id option:selected').text();
str_processing = $('#processing_id option:selected').text();
str_cuakhau = $('#cuakhau option:selected').text();

// lấy value
val_nation=$('#nation_id option:selected').data("value");
val_purpose=$('#purpose_id option:selected').data("value");
val_type=$('#type_id option:selected').data("value");
val_processing=$('#processing_id option:selected').data("value");
var sum_service=parseFloat(val_nation)+parseFloat(val_purpose)+parseFloat(val_type)+parseFloat(val_processing)+parseFloat(service_support);


$('.cf_purpose').html(str_purpose);
$('.cf_visa_type').html(str_type);
$('.cf_arrival').html(str_cuakhau);
$('.cf_process').html(str_processing);
$('.service_fee').html(sum_service+"$");
$('.cf_code').html(cf_code);
$('.total_fee').html(sum_service+fee+' USD');

$( document ).ready(function() {
    $('#nation_id, #purpose_id, #type_id, #processing_id, #cuakhau').change( function() {
		
		str_nation = $('#nation_id option:selected').text();
		str_purpose = $('#purpose_id option:selected').text();
		str_type = $('#type_id option:selected').text();
		str_processing = $('#processing_id option:selected').text();
		str_cuakhau = $('#cuakhau option:selected').text();
		// lấy value
		val_nation=$('#nation_id option:selected').data("value");
		val_purpose=$('#purpose_id option:selected').data("value");
		val_type=$('#type_id option:selected').data("value");
		val_processing=$('#processing_id option:selected').data("value");
		val_express_fee=0;
		if($("#service_support").is(":checked")){
			$('.express_fee').html("25$");
			val_express_fee=25;
		}
		else{
			$('.express_fee').html("0$");
			val_express_fee=0
		}
		var sum_service=parseFloat(val_nation)+parseFloat(val_purpose)+parseFloat(val_type)+parseFloat(val_processing)+parseFloat(val_express_fee);
	
		$('.cf_purpose').html(str_purpose);
		$('.cf_visa_type').html(str_type);
		$('.cf_arrival').html(str_cuakhau);
		$('.cf_process').html(str_processing);
		$('.service_fee').html(sum_service+"$");
		$('.cf_code').html(cf_code);
		$('.total_fee').html(sum_service+fee+' USD');
	});
	$('#service_support').change(function() {
		var express_fee=0;
		if($(this).is(":checked")){
			$('.express_fee').html("25$");
			express_fee=25;
		}
		else{
			$('.express_fee').html("0$");
			express_fee=0
		}
		str_nation = $('#nation_id option:selected').text();
		str_purpose = $('#purpose_id option:selected').text();
		str_type = $('#type_id option:selected').text();
		str_processing = $('#processing_id option:selected').text();
		str_cuakhau = $('#cuakhau option:selected').text();
		// lấy value
		val_nation=$('#nation_id option:selected').data("value");
		val_purpose=$('#purpose_id option:selected').data("value");
		val_type=$('#type_id option:selected').data("value");
		val_processing=$('#processing_id option:selected').data("value");
		var sum_service=parseFloat(val_nation)+parseFloat(val_purpose)+parseFloat(val_type)+parseFloat(val_processing)+parseFloat(express_fee);
	
		$('.cf_purpose').html(str_purpose);
		$('.cf_visa_type').html(str_type);
		$('.cf_arrival').html(str_cuakhau);
		$('.cf_process').html(str_processing);
		$('.service_fee').html(sum_service+"$");
		$('.cf_code').html(cf_code);
		$('.total_fee').html(sum_service+fee+' USD');
    });
});



function randomNumber()
{
	var str='EV';
	for(var i=0;i<5;i++)
	{
		str+= Math.floor(Math.random() * 101);
	}
	return str
}
