$(document).ready(function(){

    $(".menu-tabs li").click(function(){
        $(this).addClass('active').siblings().removeClass('active');
    });

    $("#paymentForm").change(function(){
        $(".content-pay-all").hide();
        value = $(this).val();
        id_form = "";
        if(value == 1) id_form = "transfer";
        else if(value == 2) id_form = "card";
        else if(value == 3) id_form = "golbal";
        $('#' + id_form).css("display","block");
    });

    var heightNavMobile = $(".nav-mobile").height();
    $(".box-search-mobile").css("top",heightNavMobile);
    
    $(".box-search-mobile").hide();
    $(".btn-search button").click(function(){
        $(".box-search-mobile").slideToggle();
    });

    $(".menu-mobile").css("top",heightNavMobile);

    $(".menu-mobile").hide();
    $(".btn-toggle button").click(function(){
        $(".menu-mobile").slideToggle();
    });  

});

function instructions(instructionsName) {
    var i;
    var x = document.getElementsByClassName("content-tabs-instructions-1");
    for (i = 0; i < x.length; i++) {
        x[i].style.display = "none"; 
    }
    // $(".content-tabs-instructions-1").css("display","block");
    document.getElementById(instructionsName).style.display = "block"; 
}


function openCity(cityName) {
    var i;
    var x = document.getElementsByClassName("container-tabs");
    for (i = 0; i < x.length; i++) {
        x[i].style.display = "none"; 
    }
    document.getElementById(cityName).style.display = "block"; 
}