<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['auth'])->prefix('admin')->group(function () {
    // Route::get('/', function () {
    //     return view('admin.layouts.default',['flag' => 'dashboard']);
    // })->name('dashboard');
    Route::resource('nation', 'NationController');
    Route::resource('purpose', 'PurposeController');
    Route::resource('type', 'TypeController');
    Route::resource('processing', 'ProcessingController');
    Route::resource('customer', 'CustomerController');
    Route::get('customer/detail/{id}', 'CustomerController@detailOrder')->name('detailOrder');

    Route::post('/visa/update-status/{id}', 'CustomerController@updateStatus')->name('update-status');
    Route::get('/gallery', function () {
        return view('admin.gallery',['flag' => 'gallery']);
    })->name('gallery');

    //theme option
    
    Route::prefix('theme-option')->group(function () {
        Route::get('/list', 'ThemeoptionController@indexHeader')->name('indexHeader');
        Route::get('/edit', 'ThemeoptionController@indexEdit')->name('indeEdit');
        Route::post('/edit', 'ThemeoptionController@postEdit')->name('edit-theme');
        
    });
   
});

Auth::routes();
Route::post('/step-3.html', 'HomeController@createVisa')->name('create-visa');
Route::post('/step-4.html', 'HomeController@updateVisa')->name('update-visa');
Route::get('/index.html', 'HomeController@index')->name('index');
Route::get('/', 'HomeController@index')->name('home');
Route::post('/step-1.html', 'HomeController@step1')->name('step-1');
Route::get('/step-2-evisa.html', 'HomeController@step2')->name('evisa-step-2');
Route::get('/step-2-evisa-arrival.html', 'HomeController@step2_arrival')->name('evisa-arrival-step-2');
Route::get('/procedure-instructions.html', 'HomeController@procedure')->name('procedure-instructions');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/404', function () {
        return view('errors.404',['flag' => '']);
})->name('error-404');
Route::get('/500', function () {
        return view('errors.500',['flag' => '']);
})->name('error-500');
