<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('pass_number');
            $table->string('cuakhau');
            $table->string('date_in');
            $table->string('phone');
            $table->string('email');
            $table->string('address')->nullable();
            $table->string('note')->nullable();
            $table->integer('nation_id');
            $table->integer('processing_id');
            $table->integer('purpose_id');
            $table->integer('type_id');
            $table->integer('type_tt')->nullable();
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
