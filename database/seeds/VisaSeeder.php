<?php

use Illuminate\Database\Seeder;

class VisaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('customers')->insert([
            'name' => 'Bảo Đạt',
            'email' => 'doandat0107@gmail.com',
            'pass_number' => '134578a122513',
            'cuakhau' => 'Nội Bài',
            'date_in' => '20/10/2019',
            'phone' => '098124457',
            'address' => 'Thanh Hóa',
            'note' => 'Không có gì',
            'nation_id' => '1',
            'processing_id' => '1',
            'purpose_id' => '1',
            'type_id' => '1',
            'status' => 0,
        ]);
        DB::table('customers')->insert([
            'name' => 'Anh Tuấn',
            'email' => 'anhtuank16@gmail.com',
            'pass_number' => 'ds121312sadas',
            'cuakhau' => 'Yên Bái',
            'date_in' => '30/10/2019',
            'phone' => '0931459245',
            'address' => 'Hà Nội',
            'note' => 'Không có gì',
            'nation_id' => '1',
            'processing_id' => '1',
            'purpose_id' => '1',
            'type_id' => '1',
            'status' => 1,
        ]);
    }
}
